<p>Nosotros somos el grupo de personas que decidimos participar en la construcción
y desarrollo del proyecto <b>Curso de Programación para Organizaciones
Comunitarias</b>.</p>

<p>Tenemos distintos orígenes, algunos somos estudiantes, otros trabajadores o
trabajadoras que tenemos conocimientos en tecnologías que deseamos compartir de
manera comunitaria y colaborativa.</p>

<p>Ya hemos realizado una versión del curso en que participaron estudiantes de
diversas organizaciones sociales comunitarias, de aquí también se han sumado
participantes al proyecto ya que consideran de importancia sustancial
socializar los conocimientos tecnológicos.</p>

<p>Creemos que es necesario crear un frente desde lo comunitario que sea capaz
comprender y crear tecnologías para el bienestar de lo común, de aquí sale el
término <b>tecnocomún</b>, que se desarrolla como conceptos y prácticas en torno
a lo tecnológico y comunitario.</p>

<p>Hasta el momento han participado en el proyecto:</p>

<p>La participación comprende la creación de los contenidos, enseñarlos,
trabajar en el diseño de las distintas herramientas de difusión, compartir los
conocimientos en los canales educativos de conversación</p>

<ol>
<li>David Pineda, desde 2018</li>
<li>Daniel Méndez, 2018</li>
<li>Matias Meza, desde 2019</li>
<li>Camilo Carrasco, desde 2019</li>
<li>Carolina da Silva, desde 2019</li>
<li>Raquel Bracho, desde 2019</li>
</ol>

<p>Este proyecto se puede replicar dónde sea necesario y exista la capacidad de
autogestionar una instancia del curso, para eso estará disponible esta
plataforma que (en desarrollo) permitirá gestionar los contenidos y diferentes
cursos y talleres que se den con motivo de lo <b>tecnocomún</b></p>

<p>En este sentido, también estamos trabajando en la edición de los textos guía
del curso para transformarlos en un *libro* autocontenido y didáctico con el fin
de otorgar una mejor facilidad para los procesos de aprendizaje en tecnologías libres.</p>
