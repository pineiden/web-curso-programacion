import re
import os
import ujson as json

from django.conf import settings
from django.template.loader import render_to_string

from django.core.mail import send_mail
from pathlib import Path

fields = ('subject', 'body', 'from', 'to')


def webmail_send(mail_fields, template, template_info, debug=False):
    regex_html = re.compile("\.html$")
    regex_dj_html = re.compile("\.dj\.html$")
    args = [mail_fields.get(k) for k in fields]
    body = ""
    if regex_html.search(template) or regex_dj_html.search(template):
        body = render_to_string(template, context=template_info)
    else:
        body = template
    return send_mail(*args, html_message=body, fail_silently=not debug)
