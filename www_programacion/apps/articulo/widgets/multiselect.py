from django.forms import SelectMultiple


class SelectMultipleWidget(SelectMultiple):
    template_name = 'articulo/widgets/field_select_multiple.dj.html'
    option_template_name = 'articulo/widgets/option_select.html'
    class Media:
        css = {
            'all': ('articulo/widgets/css/select_multiple.css',)
        }
        js = ('articulo/widgets/js/select_multiple.js',)
