from django.views.generic import ListView
from django.views.generic import TemplateView
from django.views.generic import DetailView
from django.views.generic import CreateView

from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q
from django.urls import reverse, reverse_lazy
from django.shortcuts import render
from articulo.forms import CrearArticuloForm

class CrearArticulo(CreateView):
    template_name = 'articulo/publicacion/crear.dj.html'
    form_class = CrearArticuloForm
    # success_url='ubicacion' -> get success url

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(*args,**kwargs)
        print("Context data", kwargs)
        return kwargs

    def post(self, request, *args, **kwargs):
        """
        Handle POST requests: instantiate a form instance with the passed
        POST variables and then check if it's valid.
        """
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

class VerArticulo(DetailView):
    template_name = 'articulo/publicacion/crear.dj.html'

class VerSerie(TemplateView):
    template_name = 'articulo/publicacion/crear.dj.html'

class ListarSerie(TemplateView):
    template_name = 'articulo/publicacion/crear.dj.html'

class CreacionExitosa(TemplateView):
    template_name = 'articulo/publicacion/crear.dj.html'

class BorrarArticulo(TemplateView):
    template_name = 'articulo/publicacion/crear.dj.html'

class BorradoExitoso(TemplateView):
    template_name = 'articulo/publicacion/crear.dj.html'

class BuscarArticulos(TemplateView):
    template_name = 'articulo/publicacion/crear.dj.html'

class ListarArticulos(TemplateView):
    template_name = 'articulo/publicacion/crear.dj.html'
