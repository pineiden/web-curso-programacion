# Create your models here.
from django.db import models
from django.utils.text import slugify
import ujson
from micro_tools.text.slug import get_unique_slug
from django.contrib.contenttypes.fields import GenericRelation
from django.conf import settings

def get_upload_articulo_file_name(instance, filename):
    return "articulo/%s/%s" % (instance.slug_titulo, filename)


def get_upload_galeria_file_name(instance, filename):
    return "articulo/%s/galeria/%s" % (instance.articulo.slug_titulo, filename)

# Create your models here.
class Serie(models.Model):
    """
    Este modelo permite definir una serie de artículos relacionados por un tema específico
    """
    titulo = models.CharField(max_length=150)
    slug = models.SlugField(unique=True)
    descripcion = models.TextField(blank=True)
    fecha_inicio = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        if not self.slug_tipo:
            self.slug_tipo = get_unique_slug(self,
                                             'slug',
                                             'titulo')
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('articulo:serie',
                       kwargs={'slug': self.slug})

    class Meta:
        app_label = "articulo"
        verbose_name = "Serie"
        verbose_name_plural = "Series"
        ordering = ("titulo","fecha_inicio")


class Clasificacion(models.Model):
    """
    Este modelo permite generar una clasificación genérica a dos niveles:
    categoría y etiqueta
    De manera que sea posible agrupar según lo que corresponda
    """
    CHOICES=[
        ("group",'categoria'),
        ("tag","etiqueta")
    ]
    opcion = models.CharField(
        max_length=10,
        choices=CHOICES)
    tipo = models.CharField(
        max_length=20,
        unique=True)
    slug_tipo = models.SlugField(unique=True)
    descripcion = models.TextField(blank=True)

    def get_absolute_url(self):
        return reverse('articulo:clasificacion',
                       kwargs={'slug_tipo': self.slug_tipo})

    def save(self, *args, **kwargs):
        if not self.slug_tipo:
            self.slug_tipo = get_unique_slug(self,
                                             'slug_tipo',
                                             'tipo')
        super().save(*args, **kwargs)

    class Meta:
        app_label = "articulo"
        verbose_name = "Clasificación"
        verbose_name_plural = "Clasificaciones"
        ordering = ("tipo",)

    def __repr__(self):
        return json.dumps(self.__dict__)

    def __str__(self):
        return self.tipo

from .model_diff import ModelDiffMixin
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from django.utils import timezone

class ControlDateTime(models.Model):
    """
    Este modelo permite gestionar de manera más declarativa y al detalle las
    etapas de publicación del artículo
    """
    fecha_creacion = models.DateTimeField(auto_now=True)
    fecha_fin_borrador = models.DateTimeField(auto_now=True)
    fecha_aprobacion = models.DateTimeField(auto_now=True)
    fecha_publicacion = models.DateTimeField(auto_now=True)
    fecha_cancelacion = models.DateTimeField(blank=True, null=True)

    class Meta:
        app_label = "articulo"
        verbose_name = "Control de Fechas"
        verbose_name_plural = "Controles de Fechas"
        ordering = ("fecha_creacion",)

    def __repr__(self):
        return json.dumps(self.__dict__)

    def __str__(self):
        return self.tipo


class Articulo(ModelDiffMixin, models.Model):
    STATUS_CHOICES=[
        ('draft', 'borrador'),
        ('review', 'revision'),
        ('public', 'publicado'),
        ('canceled','cancelado')
    ]
    titulo = models.CharField(max_length=200)
    slug_titulo = models.SlugField()
    sub_titulo = models.CharField(
        help_text="Subtítulo",
        max_length=200, blank=True, null=True)
    imagen = models.ImageField(
        upload_to=get_upload_articulo_file_name,
        blank=True)
    contenido = models.TextField(help_text="Escribe todo el contenido")
    resumen = models.TextField(default="Escribe un resumen")
    status = models.CharField(max_length=10,
                              choices=STATUS_CHOICES,
                              default="draft")
    serie = models.ForeignKey(
        Serie,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="articulos",
        related_query_name="articulo",
    )
    clasificacion = models.ForeignKey(
        Clasificacion,
        on_delete=models.PROTECT,
        blank=True,
        null=True,
        related_name="articulos",
        related_query_name="articulo",
        limit_choices_to={'opcion': "group"})
    etiqueta = models.ManyToManyField(
        Clasificacion,
        blank=True,
        related_name="articulos_tag",
        related_query_name="articulo_tag",
        limit_choices_to={'opcion': "tag"})
    date_control = models.ForeignKey(ControlDateTime,
                                     on_delete=models.CASCADE,
                                     blank=True,
                                     null=True,
                                    related_name="articulos",
                                    related_query_name="articulo",)
    # relación generica a algun modelo posible
    relacionado_con =  models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE,
        related_name="articulos",
        related_query_name="articulo")
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('relacionado_con', 'object_id')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__past = self

    def get_absolute_url(self):
        return reverse('articulo:ver',
                       kwargs={'slug': self.slug_titulo})


    def control_datetime(self, **kwargs):
        if not self.date_control:
            # primera vez
            dt_control=settings.DATE_CONTROL
            control=kwargs.get('control')
            dt_control.update(control)
            dt_now=timezone.now()
            params=settings.date_control_params(dt_now, control=dt_control)
            dt=DateControl(**params)
            dt.save()
            self.date_control=dt
        else:
            # instancia ya creada
            dt_control=settings.DATE_CONTROL
            control=kwargs.get('control')
            params=settings.date_control_params(
                self.date_control.fecha_creacion, control=dt_control)
            for field, value in params.items():
                setattr(self.date_control, field, value)
            self.date_control.save()

    def save(self, *args, **kwargs):
        self.control_datetime(**kwargs)
        if not self.slug_titulo:
            self.slug_titulo = get_unique_slug(self,
                                               'slug_titulo',
                                               'titulo')
        super().save(*args, **kwargs)

    class Meta:
        app_label = "articulo"
        verbose_name = "Artículo"
        verbose_name_plural = "Artículos"
        ordering = ("clasificacion", "titulo")

    def __str__(self):
        return self.titulo

    def __repr__(self):
        return json.dumps(self.__dict__)



from django.contrib.postgres.fields import JSONField, ArrayField

class Modificacion(models.Model):
    articulo=models.ForeignKey(Articulo,
                               on_delete=models.PROTECT)
    campos=JSONField()
    fecha_modificacion=models.DateTimeField(auto_now=True)
    comentario = models.CharField(max_length=300)

    class Meta:
        app_label = "articulo"
        verbose_name = "Modificacion"
        verbose_name_plural = "Modificaciones"
        ordering = ("fecha_modificacion", "articulo")

    def __repr__(self):
        return json.dumps(self.__dict__)

    def __str__(self):
        return self.tipo

class Galeria(models.Model):
    articulo = models.ForeignKey(Articulo,
                                 on_delete=models.PROTECT)
    nombre=models.CharField(max_length=50)
    slug = models.SlugField()
    fecha_creacion=models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        if not self.slug_titulo:
            self.slug_titulo = get_unique_slug(self,
                                               'slug',
                                               'nombre')
        # TODO: check if exists before save
        super().save(*args, **kwargs)

    class Meta:
        app_label = "articulo"
        verbose_name = "Galeria"
        verbose_name_plural = "Galerias"
        ordering = ("fecha_creacion", "nombre")

    def __str__(self):
        return self.nombre

    def __repr__(self):
        return json.dumps(self.__dict__)

class ImagenGaleria(models.Model):
    galeria=models.ForeignKey(Galeria,
                              on_delete=models.CASCADE)
    imagen = models.ImageField(
        upload_to=get_upload_galeria_file_name,
        blank=True)
    alt= models.CharField(max_length=100)
    height= models.IntegerField(default=200)
    width=models.IntegerField(default=300)
    css_class = models.CharField(max_length=100,
                                 default="img_articulo")
    css_id= models.CharField(max_length=100)
    fecha_insercion=models.DateTimeField(auto_now=True)


    class Meta:
        app_label = "pagina"
        verbose_name = "Clasificación"
        verbose_name_plural = "Clasificaciones"
        ordering = ("tipo",)

    def __repr__(self):
        return json.dumps(self.__dict__)

    def __str__(self):
        return self.tipo

from django.dispatch import receiver
from django.db.models.signals import pre_save


@receiver(pre_save, sender=Articulo)
def article_modified(sender, instance, **kwargs):
    try:
        obj = sender.objects.get(pk=instance.pk)
    except sender.DoesNotExist:
        pass # Object is new, so field hasn't technically changed, but you may want to do something else here.
    else:
        if not instance.has_changed:
            opts={
                'articulo':instance,
                'campos':instance.diff
            }
            mod=Modificacion(**opts)
            mod.save()
