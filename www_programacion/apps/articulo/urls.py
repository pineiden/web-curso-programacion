from django.conf.urls import url
from django.urls import path
# from django.contrib import admin
from .views import *

app_name = 'articulo'

urlpatterns = [
    path('crear',
         CrearArticulo.as_view(),
         name='crear'),
    path('<slug:slug_titulo>',
         VerArticulo.as_view(),
         name='ver'),
    path('serie/<slug:slug>',
         VerSerie.as_view(),
         name='serie'),
    path('series',
         ListarSerie.as_view(),
         name='series'),
    path('creacion/exitosa/id=<int:id>',
         CreacionExitosa.as_view(),
         name='creacion-exitosa'),
    path('actualizar/id=<int:id>',
         CrearArticulo.as_view(),
         name='actualizar'),
    path('borrar/id=<int:id>',
         BorrarArticulo.as_view(),
         name='actualizar'),
    path('borrado/exitoso/id=<int:id>',
         BorradoExitoso.as_view(),
         name='borrado-exitoso'),
    path('buscar',
         BuscarArticulos.as_view(),
         name='buscar'),
    path('listar',
         ListarArticulos.as_view(),
         name='listar'),
    path('listar/ultimos/cantidad=<int:cantidad>',
         ListarArticulos.as_view(),
         name='listar-ultimos'),
    #path('listar/inicio=<float:inicio>/final=<float:final>',
    #     ListarArticulos.as_view(),
    #     name='listar'),
    #path('listar/campo=<str:campo>/opciones=<list:opciones>',
    #     ListarArticulos.as_view(),
    #     name='listar-filtro'),
]
