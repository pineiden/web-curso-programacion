from django import forms
from articulo.models import Articulo
from articulo.widgets import SelectMultipleWidget
from articulo.widgets import MarkdownTextAreaWidget

class CrearArticuloForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['titulo'].widget.attrs.update(
            {'class': 'input'})
        self.fields['sub_titulo'].widget.attrs.update(
            {'class': 'input'})
        self.fields['imagen'].widget.attrs.update(
            {'class': 'file-input'})
        self.fields['contenido'].widget.attrs.update(
            {'class': 'textarea'})
        self.fields['resumen'].widget.attrs.update(
            {'class': 'textarea'})
        self.fields['serie'].widget.attrs.update(
            {'class': 'select', 'input_name':"select"})
        self.fields['status'].widget.attrs.update(
            {'class': 'select', 'input_name':"select"})
        self.fields['clasificacion'].widget.attrs.update(
            {'class': 'select', 'input_name':"select"})
        self.fields['etiqueta'].widget.attrs.update(
            {'class': 'select',
             'input_name':"select",
             'multiple':'multiple'})
        self.fields['relacionado_con'].widget.attrs.update(
            {'class': 'select', 'input_name':"select"})
        self.fields['object_id'].label="Id de objeto"
        self.fields['object_id'].widget.attrs.update(
            {'class': 'input'})


    class Meta:
        model = Articulo
        exclude = ["date_control","slug_titulo"]
        widgets = {
            'etiqueta': SelectMultipleWidget(attrs={'class':
        'etiqueta-select'}),
            'contenido': MarkdownTextAreaWidget(attrs={'class':
        'md-textarea'})
        }

