from django.db import models
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField

# Create your models here.


class Propuesta(models.Model):
    """
    Este modelo permite a los usuarios proponer algún curso, describiendo la
    idea, incluyendo el documento y comprometiendose a:
    - llevar a cabo
    - capacitarse en preparar
    - disponer de los contenidos para que se replique
    """
    ON_REVIEW = "on_review"
    ACCEPTED = "accepted"
    REJECTED = "rejected"
    DEFAULT_MSG = "Tu propuesta fue..."
    ACEPTA_DEFAULT = "¡La propuesta ha sido aceptada!"
    RECHAZA_DEFAULT = "¡La propuesta ha sido rechazada!"
    STATUS_CHOICES = [(ON_REVIEW, "Revisión"), (ACCEPTED, "Aceptada"),
                      (REJECTED, "Rechazada")]
    usuario = models.ForeignKey(User,
                                related_name="propuestas",
                                on_delete=models.SET_NULL,
                                blank=True,
                                null=True)
    documento = models.FileField()
    descripcion = models.TextField()
    nombre = models.CharField(max_length=200)
    slug_nombre = models.SlugField(default=None, blank=True)
    status = models.CharField(max_length=30,
                              choices=STATUS_CHOICES,
                              default=ON_REVIEW)
    date_time = models.DateTimeField(auto_now=True)
    acepta = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        self.slug_nombre = self.check_slugs()
        # TODO: check if exists before save
        instance = super().save(*args, **kwargs)
        return instance

    def check_slugs(self):
        st = slugify(self.tipo)
        slugs = Propuesta.objects.filter(slug_tipo=st)
        while slugs:
            st = slugify("%s_x" % self.tipo)
            slugs = Propuesta.objects.filter(slug_tipo=st)
        return st

    class Meta:
        app_label = "propuesta"
        verbose_name = "Propuesta"
        verbose_name_plural = "Propuestas"
        ordering = ("nombre", "-date_time", "usuario")

    def __repr__(self):
        return f"Propuesta({self.nombre},{self.usuario},{self.date_time})"

    def __str__(self):
        return self.nombre


class RevisionPropuesta(models.Model):
    def DEFAULT_JSON():
        return dict(usuario=[], admins=[])

    DEFAULT_MSG = "Tu propuesta fue..."
    ACEPTA_DEFAULT = "¡La propuesta ha sido aceptad@!"
    RECHAZA_DEFAULT = "¡La propuesta ha sido rechazad@!"
    promesa = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date_time = models.DateTimeField(auto_now=True)
    propuesta = models.ForeignKey(Propuesta, on_delete=models.CASCADE)
    mensaje = models.TextField(default=DEFAULT_MSG, blank=True)
    enviados = JSONField(default=DEFAULT_JSON)

    class Meta:
        app_label = "propuesta"
        verbose_name = "Revisión de Propuesta"
        verbose_name_plural = "Revisiones de Propusta"
        ordering = (
            '-date_time',
            "propuesta",
            'user',
        )
        permissions = [("can_review", "Puede revisar")]

    def __str__(self):
        return f"{self.user.profile}->{self.propuesta}:{self.date_time}"

    def __repr__(self):
        return f"{self.user.profile}->{self.propuesta}:{self.date_time}"
