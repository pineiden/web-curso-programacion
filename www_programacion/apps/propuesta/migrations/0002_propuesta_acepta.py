# Generated by Django 3.0 on 2020-09-15 02:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('propuesta', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='propuesta',
            name='acepta',
            field=models.BooleanField(default=False),
        ),
    ]
