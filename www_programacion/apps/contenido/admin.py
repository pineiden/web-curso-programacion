from django.contrib import admin
from .models import (ContenidosProyecto, Sesion, Contenido,
                     SettingsNotificacion, Proyecto)

# Register your models here.
admin.site.register(ContenidosProyecto)
admin.site.register(Sesion)
admin.site.register(Contenido)
admin.site.register(SettingsNotificacion)
