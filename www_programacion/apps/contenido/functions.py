import csv
from io import StringIO
from datetime import datetime as dt

DATE_FORMAT = "%d/%m/%Y"
TIME_FORMAT = "%H:%M:%S"


def get_date(date_str):
    try:
        _date = dt.strptime(date_str, DATE_FORMAT).date()
        return _date
    except Exception as e:
        print(f"Exception {e}")
        return dt.today().date()


def get_time(time_str):
    try:
        print("Time str", time_str)
        _time = dt.strptime(time_str, TIME_FORMAT).time()
        return _time
    except Exception as e:
        print(f"Exception {e}")
        return dt.today().time()


def load_data(filefield):
    """
    Lee csv, cada fila será un dict
    Revisa si el dato en csv existe, entonces lo reemplaza en json
    """
    fields = ["posicion", "fecha", "titulo", "resumen", "hora", "modo"]
    sesiones = {}
    with StringIO(filefield.read().decode()) as programa:
        reader = csv.DictReader(programa, delimiter=';')
        for row in reader:
            row["posicion"] = int(row["posicion"])
            filtered = {
                key: value
                for key, value in row.items() if key in fields
            }
            sesiones[filtered.get("posicion")] = filtered
    return sesiones


def email_notificacion(settings):
    today = dt.today().date()
    template_subject = "email/subject.dj.html"
    template_notificacion = "email/notificacion.dj.html"
    contenidos = settings.contenidos
    protecto = contenidos.proyecto
    curso = proyecto.curso
    medio = settings.medio
    usuario = settings.usuario
    sesiones = contenidos.sesiones.all()
    for sesion in sesiones:
        ejercicio = sesion.contenidos.filter(modo='EJ').first()
        clase = sesion.contenidos.filter(modo='CL').first()
        if medio == '@':
            context = {
                "curso": curso.nombre,
                "hora": sesion.hora.isoformat(),
                "fecha": sesion.fecha.isoformat(),
                "ejercicio": ejercicio,
                "clase": clase,
                "modos":
                [sesion.CHOICES_DICT.get(c) for c in list(sesion.modos)]
            }
