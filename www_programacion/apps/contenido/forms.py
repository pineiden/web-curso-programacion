from django import forms


class SearchContentForm(forms.Form):
    busqueda = forms.CharField(label='Búsqueda', max_length=200)
