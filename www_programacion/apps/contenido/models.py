from django.utils.functional import lazy
from datetime import time
from django.db import models
from django.conf import settings
if 'sqlite3' in settings.DATABASES["default"]["ENGINE"]:
    from jsonfield import JSONField
else:
    from django.contrib.postgres.fields import JSONField
# Create your models here.
#from apps.proyecto.models import Proyecto

from django.db.models.signals import post_save
from django.dispatch import receiver

from datetime import datetime as dt
from django.utils.timezone import datetime
from django.utils.dateparse import parse_date
from django.utils.timezone import is_aware, make_aware
from .validators import validate_file_extension
from .functions import load_data, get_date, get_time
import os
from django.utils.text import slugify
from groups_manager.models import (Group, Member, GroupEntity, GroupMember,
                                   GroupMemberRole)
from django.contrib.auth.models import User

from apps.proyecto.models import Proyecto
from django.urls import reverse


def file_upload_programa(instance, filename):
    try:
        proyecto = instance.proyecto
        curso = proyecto.curso
        if getattr(curso, 'slug', None):
            curso_slug = curso.slug
        else:
            curso_slug = slugify(curso)
        if getattr(proyecto, 'slug', None):
            generacion = proyecto.slug
        else:
            generacion = slugify(proyecto)
        return f"{curso_slug}/{generacion}/programa/{filename}"
    except Exception as ex:
        raise ex


class ContenidosProyecto(models.Model):
    """
    JSON debe contener las rutas al html principal de cada sesión
    Por cada 'save' de contenidos proyecot se debe crear de manera automatica
    las sesiones descritas en parametros
    """
    def default_json():
        DEFAULT_JSON = {
            'sesiones': {
                1: {
                    "posicion": 1,
                    "fecha": 'd/m/Y',
                    "titulo": "Nombre sesión",
                    "resumen": "resumen"
                }
            }
        }
        return DEFAULT_JSON

    proyecto = models.OneToOneField(Proyecto,
                                    related_name='contenido',
                                    on_delete=models.SET_NULL,
                                    null=True)
    parametros = JSONField(default=default_json)
    programa = models.FileField(upload_to=file_upload_programa,
                                null=True,
                                max_length=1200,
                                blank=True,
                                validators=[validate_file_extension])
    actualizar = models.BooleanField(default=True)
    max_notificaciones = models.PositiveIntegerField(default=3)
    notificar = models.BooleanField(default=True)
    cantidad_clases = models.PositiveIntegerField(default=10)

    def __repr__(self):
        return f"Contenidos|{self.proyecto.curso}|{self.proyecto}"

    def __str__(self):
        return f"Contenidos|{self.proyecto.curso}|{self.proyecto}"

    def save(self, *args, **kwargs):
        self.merge_params()
        instance = super().save(*args, **kwargs)

    @property
    def programa_ext(self):
        if isinstance(self.programa.name, str):
            ext = self.programa.name.split(".")[-1]
            return ext
        else:
            return ""

    def merge_params(self):
        """
        Update new information from CSV to field
        """
        clase = type(self)
        anterior = None
        if self.id:
            anterior = clase.objects.get(id=self.id)
        #
        if self.programa_ext == 'csv':
            if anterior:
                if self.programa != anterior.programa or self.actualizar:
                    self.load_data()
            else:
                self.load_data()

    def load_data(self):
        """
        Lee csv, cada fila será un dict
        Revisa si el dato en csv existe, entonces lo reemplaza en json
        """
        if 'sesiones' not in self.parametros:
            self.parametros["sesiones"] = {}
        fields = ["posicion", "fecha", "titulo", "resumen", "hora", "modo"]
        sesiones = load_data(self.programa)
        self.parametros["sesiones"].update(sesiones)


@receiver(post_save, sender=ContenidosProyecto)
def create_sessions(sender, instance, created, raw, using, **kwargs):
    sessions = instance.parametros.get("sesiones", {})
    print("Sesiones->", sessions)
    for position, session_param in sessions.items():
        print("Position", position)
        sessions = Sesion.objects.filter(contenidos_proyecto=instance)
        if sessions.count() <= instance.cantidad_clases:
            _date = session_param.get('fecha')
            _hora = session_param.get('hora')
            print("Date", _date, "Hora", _hora)
            session_param["fecha"] = get_date(_date)
            session_param["hora"] = get_time(_hora)
            print("SESSION PARAM", session_param)
            session, creada = Sesion.objects.get_or_create(
                contenidos_proyecto=instance, **session_param)


class SettingsNotificacion(models.Model):
    """
    Notificaciones configuradas para el usuario
    """
    MENSAJE = """
    Recuerda {{nombre_completo}}, a las {{hora}} de hoy {{dia}}
    tienes la clase del curso {{curso}}.
    """
    VARIABLES = """
    nombre_completo :: tu nombre
    hora :: hora de inicio de clases
    dia :: dia de la clase
    curso :: nombre del curso 
    """
    HORA_ENVIO = time(8)
    TIEMPO = 60
    EMAIL = '@'
    TELEGRAM = 'TG'
    CHOICES = [(EMAIL, "E-mail"), (TELEGRAM, "telegram")]
    contenidos = models.ForeignKey(ContenidosProyecto,
                                   related_name='notificaciones',
                                   on_delete=models.CASCADE)
    medio = models.CharField(max_length=10, choices=CHOICES, default=EMAIL)
    mensaje = models.TextField(default=MENSAJE)
    repetir = models.BooleanField(default=False)
    periodo_tpo = models.PositiveIntegerField(default=TIEMPO)
    horas_antes = models.PositiveIntegerField(default=10)
    hora_envio = models.TimeField(default=HORA_ENVIO)
    miembro = models.ForeignKey(Member,
                                related_name='notificaciones',
                                on_delete=models.CASCADE,
                                null=True,
                                blank=True)


class Sesion(models.Model):
    """
    Por ContenidoProyecto : Sesiones

    unique_key: ["sesion", "posicion"]

    Al guardar-> Crear tareas programadas de:
    envíar emails a estudiantes  y miembros
    tb. Telegram msg o wsp

    Que se muestre la opcion en perfil: de manera general
    settings: configuración por sesion por defecto 1 email

    Limitar según la cantidad y valores disponibles:
    cont_proyecto = ?
    ids_sesion = {s.id for s in cont_proyecto.sesiones.all()}
    ids_av = {i+1 for i in range(cont_proyecto.cantidad_clases)}
    available = ids_av - ids_sesion
    """
    TIME_INICIO = get_time("18:00:00")
    CHOICE_MODE = [("O", "online"), ("P", "presencial"), ("OP", "mixto")]
    CHOICES_DICT = dict(CHOICE_MODE)
    posicion = models.PositiveIntegerField(default=0, blank=True, null=True)
    contenidos_proyecto = models.ForeignKey(ContenidosProyecto,
                                            related_name='sesiones',
                                            on_delete=models.SET_NULL,
                                            null=True)
    titulo = models.CharField(max_length=250)
    slug_titulo = models.SlugField(null=True, blank=True, max_length=250)
    fecha = models.DateField()
    hora = models.TimeField(default=TIME_INICIO)
    modo = models.CharField(max_length=2, choices=CHOICE_MODE, default='O')
    url_online = models.URLField(max_length=1000,
                                 default='https://www.cursodeprogramacion.cl')
    lugar = models.CharField(max_length=100, default="Santiago, Chile")
    realizada = models.BooleanField(default=False)
    resumen = models.TextField()

    def __repr__(self):
        return f"{self.posicion}:{self.titulo}"

    def __str__(self):
        return f"{self.posicion}:{self.titulo}"

    def save(self, *args, **kwargs):
        self.slug_titulo = slugify(self.titulo)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        posicion = self.posicion
        curso = self.contenidos_proyecto.proyecto.curso.slug
        generacion = self.contenidos_proyecto.proyecto.slug
        kwargs = {
            "posicion": posicion,
            "curso": curso,
            "generacion": generacion
        }
        return reverse('contenido:sesion_main', kwargs=kwargs)

    class Meta:
        ordering = ["posicion"]


def file_upload(instance, filename):
    try:
        proyecto = instance.sesion.contenidos_proyecto.proyecto
        curso = proyecto.curso.slug_nombre
        generacion = proyecto.slug_generacion
        sesion = instance.sesion.slug_titulo
        tipo = instance.tipo
        if proyecto:
            return f"{curso}/{generacion}/{sesion}/{tipo}/{filename}"
        else:
            return f"proyecto_perdido/{tipo}/{filename}"
    except Exception as ex:
        raise ex


def file_upload_torrent(instance, filename):
    try:
        proyecto = instance.sesion.contenidos_proyecto.proyecto
        curso = proyecto.curso.slug_nombre
        generacion = proyecto.slug_generacion
        sesion = instance.sesion.slug_titulo
        tipo = "torrent"
        if proyecto:
            return f"{curso}/{generacion}/{sesion}/{tipo}/{filename}"
        else:
            return f"proyecto_perdido/{tipo}/{filename}"
    except Exception as ex:
        raise ex


class Contenido(models.Model):
    """
    Por sesión:  guia, ejercicio
    """
    CLASE = 'CL'
    EJERCICIO = 'EJ'
    CONTENIDO_CHOICES = [(CLASE, "clase"), (EJERCICIO, "ejercicio")]
    DICT_CHOICES = dict(CONTENIDO_CHOICES)

    sesion = models.ForeignKey(Sesion,
                               related_name='contenidos',
                               on_delete=models.SET_NULL,
                               null=True)
    tipo = models.CharField(max_length=10,
                            choices=CONTENIDO_CHOICES,
                            default=CLASE)
    video_url = models.URLField(max_length=2000, null=True, blank=True)
    torrent = models.FileField(upload_to=file_upload_torrent,
                               null=True,
                               max_length=1200,
                               blank=True)
    resumen = models.TextField()
    documento = models.FileField(upload_to=file_upload,
                                 null=True,
                                 max_length=1200,
                                 blank=True)
    date_time = models.DateTimeField(auto_now=True)
    date_time_updated = models.DateTimeField(auto_now_add=True)
    guia = models.ForeignKey(Member,
                             on_delete=models.SET_NULL,
                             null=True,
                             blank=True)

    def __str__(self):
        return f"{self.DICT_CHOICES.get(self.tipo)}|{self.sesion}|{self.date_time}"

    def __repr__(self):
        return f"Contenido({self.DICT_CHOICES.get(self.tipo)}|{self.sesion}|{self.date_time})"
