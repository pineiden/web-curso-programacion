from .models import RemoveMemberFromGroup
from .models import Admin
from django import forms
from groups_manager.forms import GroupForm
from groups_manager.models import Group, Member
from django.http import HttpResponseRedirect


class AdminGroupForm(GroupForm):
    def save(self, commit=True):
        instance = super().save(commit=False)
        admin = Admin(group=instance)
        admin.save()
        instance.save()
        return instance


class GroupMemberForm(forms.Form):
    member = forms.ChoiceField(label='Miembro', choices=[])
    roles = forms.MultipleChoiceField(label="Rol", choices=[])

    def __init__(self, *args, **kwargs):
        instance = kwargs.get('instance')
        if instance:
            del kwargs["instance"]
        super().__init__(*args, **kwargs)
        initial = kwargs.get('initial', {})
        self.fields["member"].initial = kwargs.get('initial', {}).get('member')
        self.fields["roles"].initial = kwargs.get('initial', {}).get('roles')

        self.fields["member"].choices = [(m.id, m.username)
                                         for m in Member.objects.all()]
        if initial:
            self.fields["member"].widget.attrs["disabled"] = True

        if instance:
            self.fields["roles"].choices = [(c.id, c.name)
                                            for c in instance.sub_grupos()]


class RemoveMemberGroupForm(forms.ModelForm):
    class Meta:
        model = RemoveMemberFromGroup
        fields = ["member", "groups", "responsable", "reason", "promesa"]

        widgets = {
            'member': forms.HiddenInput,
            'groups': forms.HiddenInput,
            'user': forms.HiddenInput,
            'responsable': forms.HiddenInput,
        }

        labels = {"reason": "Motivo"}
