from django.contrib.auth.models import User
from groups_manager.models import Group, Member
from groups_manager.models import group_save, group_delete
from django.db.models.signals import post_save, post_delete
from django.db import models
from groups_manager.models import Group, GroupType
from django.utils.text import slugify
# Create your models here.


class AdminGroup(Group):
    class Meta:
        app_label = "admin_group"
        verbose_name = "Admin Group"
        verbose_name_plural = "Admin Groups"
        proxy = True

    def save(self, *args, **kwargs):
        if not self.group_type:
            self.group_type = GroupType.get_or_create(label='Admin').first()
        super().save(*args, **kwargs)


class Admin(models.Model):
    slug_name = models.SlugField()
    group = models.OneToOneField(AdminGroup, on_delete=models.CASCADE)

    class Meta:
        app_label = "admin_group"
        verbose_name = "Admin"
        verbose_name_plural = "Admins"

    def save(self, *args, **kwargs):
        self.slug_name = slugify(self.group.name)
        super().save(*args, **kwargs)


post_save.connect(group_save, sender=AdminGroup)
post_delete.connect(group_delete, sender=AdminGroup)


class RemoveMemberFromGroup(models.Model):
    member = models.ForeignKey(Member,
                               related_name='removed',
                               on_delete=models.SET_NULL,
                               blank=True,
                               null=True)
    groups = models.ManyToManyField(
        Group,
        related_name='removed_members',
        blank=True,
    )
    responsable = models.ForeignKey(User,
                                    related_name='removed_responsable',
                                    on_delete=models.SET_NULL,
                                    blank=True,
                                    null=True)
    reason = models.TextField()
    promesa = models.BooleanField(default=False)

    class Meta:
        app_label = "admin_group"
        verbose_name = "Remove Member"
        verbose_name_plural = "Remove Members"

    def __repr__(self):
        return f"RemoveMemberFromGroup(self.member, self.groups.all())"

    def __str__(self):
        return f"{self.user} remueve {self.member} de {self.groups.all()}"

    def save(self, *args, **kwargs):
        print("args", args, "kwargs", kwargs)
        super().save(*args, **kwargs)
        print("Grupos on save...", self.groups.all())
        print(self.member, self.responsable, self.reason, self.promesa)
        for group in self.groups.all():
            print("Removienod member", self.member, group)
            try:
                group.remove_member(self.member)
                group.save()
            except Exception as ex:
                print(ex)
