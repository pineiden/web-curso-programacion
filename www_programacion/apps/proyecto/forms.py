from django import forms
from django_json_widget.widgets import JSONEditorWidget
from .models import EmailTemplates, Curso, Proyecto


class CursoForm(forms.ModelForm):
    class Meta:
        model = Curso
        fields = [
            "nombre", "imagen", "resumen", "descripcion", "clasificacion",
            "usuario"
        ]
        widgets = {'usuario': forms.HiddenInput}


class ProyectoForm(forms.ModelForm):
    fecha_inscripcion_inicio = forms.DateField(
        widget=forms.HiddenInput(attrs={"id": "fecha_inscripcion_inicio"}),
        input_formats=['%d/%m/%Y'])
    fecha_inscripcion_final = forms.DateField(
        widget=forms.HiddenInput(attrs={"id": "fecha_inscripcion_final"}),
        input_formats=['%d/%m/%Y'])
    fecha_inicio = forms.DateField(
        widget=forms.HiddenInput(attrs={"id": "fecha_inicio"}),
        input_formats=['%d/%m/%Y'])
    fecha_final = forms.DateField(
        widget=forms.HiddenInput(attrs={"id": "fecha_final"}),
        input_formats=['%d/%m/%Y'])

    class Meta:
        model = Proyecto
        fields = [
            "curso", "valor", "generacion", "imagen_totem", "icono_totem",
            "afiche", "imagen_fondo", "resumen", "descripcion", "fecha_inicio",
            "fecha_final", "fecha_inscripcion_inicio",
            "fecha_inscripcion_final", "usuario"
        ]
        widgets = {'usuario': forms.HiddenInput}


class EmailTemplatesForm(forms.ModelForm):
    class Meta:
        model = EmailTemplates

        fields = ["proyecto", "parametros"] + list(
            map(lambda x: f"inscripcion_{x}", [
                "exitosa_usuario", "exitosa_admin", "rechazo", "acepta",
                "result_admins"
            ]))

        widgets = {'parametros': JSONEditorWidget(width="600px")}
