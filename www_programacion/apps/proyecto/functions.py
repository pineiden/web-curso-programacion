from .models import Proyecto
from django.utils.timezone import datetime
"""
Proyectos activos al momento de la consulta
"""


def submit_active_projects(*args, **kwargs):
    today = datetime.today().date()
    queryset = Proyecto.objects.filter(fecha_inscripcion_inicio__lte=today,
                                       fecha_inscripcion_final__gte=today)
    return queryset


def happening_active_projects(*args, **kwargs):
    today = datetime.today().date()
    queryset = Proyecto.objects.filter(fecha_inicio__lte=today,
                                       fecha_final__gte=today)
    return queryset


def passed_projects(*args, **kwargs):
    today = datetime.today().date()
    queryset = Proyecto.objects.filter(fecha_final__lt=today)
    return queryset
