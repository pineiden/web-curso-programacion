from django.contrib.syndication.views import Feed
from django.urls import reverse
from .models import Proyecto


class LatestProjects(Feed):
    title = 'Proyectos recientes'
    link = 'proyecto/feed'
    description = 'Subscripción para estar al día con nuevos proyectos'

    def items(self):
        return Proyecto.objects.order_by('-fecha_inicio')[:5]

    def item_title(self, item):
        return item.curso.nombre

    def item_description(self, item):
        return "%s: %s" % (item, item.curso.descripcion)

    # item_link is only needed if NewsItem has no get_absolute_url method.
    def item_link(self, item):
        return reverse('proyecto:course_generation',
                       kwargs={
                           'slug_curso': item.curso.slug_nombre,
                           'slug_generacion': item.slug_generacion
                       })
