from django.contrib import admin
from apps.proyecto.models import (Clasificacion, Proyecto, Curso, ValorCurso,
                                  EmailTemplates)
# Register your models here.

admin.site.register(Clasificacion)
admin.site.register(Proyecto)
admin.site.register(Curso)
admin.site.register(ValorCurso)
admin.site.register(EmailTemplates)
