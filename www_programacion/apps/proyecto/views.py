from .models import get_member
from django.utils.timezone import datetime  # important if using timezones
from django.contrib.auth.models import User
from .forms import EmailTemplatesForm, CursoForm, ProyectoForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect
from networktools.library import my_random_string
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from datetime import datetime as dt, date, time
from files.functions.links import LinkData
from apps.inscripcion.models import Inscripcion, ChequeoCodigo
from apps.proyecto.models import (Proyecto, Curso, Clasificacion, ValorCurso,
                                  EmailTemplates)
from apps.home.views import menu_nav
from apps.admin_group.functions import my_cursos, my_proyectos, get_member
"""
Generic Views
"""
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import TemplateView
from django.views.generic import CreateView
from django.views.generic import DeleteView
from django.views.generic import UpdateView
"""
app imports
"""
"""
Other imports
"""
# from django.contrib.messages.views import SuccessMessageMixin


def app_menu():
    menu = {
        "Clasificación": [
            LinkData("Listar",
                     "proyecto:clasification_list",
                     model="clasificacion",
                     perms=["view"]),
            LinkData("Crear",
                     "proyecto:clasification_create",
                     model="clasificacion",
                     perms=["add"]),
        ],
        "Curso": [
            LinkData("Listar",
                     "proyecto:course_list",
                     model="curso",
                     perms=["view"]),
            LinkData("Crear",
                     "proyecto:course_create",
                     model="curso",
                     perms=["add"]),
        ],
        "Proyecto": [
            LinkData("Listar",
                     "proyecto:project_list",
                     model="proyecto",
                     perms=["view"]),
            LinkData("Crear",
                     "proyecto:project_create",
                     model="proyecto",
                     perms=["add"]),
        ],
        "Email": [
            LinkData("Listar",
                     "proyecto:emails_list",
                     model="emailtemplates",
                     perms=["view"]),
            LinkData("Crear",
                     "proyecto:emails_create",
                     model="emailtemplates",
                     perms=["add"]),
        ],
        "Valor": [
            LinkData("Listar",
                     "proyecto:coursevalue_list",
                     model="valorcurso",
                     perms=["view"]),
            LinkData("Crear",
                     "proyecto:coursevalue_create",
                     model="valorcurso",
                     perms=["add"]),
        ],
    }
    return menu


"""
Base :::
"""


class ProjectBase:
    app_active = "Proyecto"

    def menu(self):
        this_menu = app_menu()
        return this_menu


"""
Project
"""


class ProjectAppView(LoginRequiredMixin, ProjectBase, TemplateView):
    template_name = 'proyecto/app.dj.html'

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active
        })
        return kwargs


class ProjectListView(LoginRequiredMixin, ProjectBase, ListView):
    """
    TODO:
    Listar proyectos de curso activos para inscripción
    Configurar alertas:
    AZUL-VERDE-AMARILLO-ROJO
    >1mes,1mes,2sem,1sem
    """
    template_name = 'proyecto/proyecto/list.dj.html'
    model = Proyecto
    today_min = dt.combine(date.today(), time.min)

    def get_context_data(self, *args, **kwargs):
        today_min = self.today_min
        kwargs = super().get_context_data(**kwargs)
        proyectos_inscripcion_activa = Proyecto.objects.filter(
            Q(fecha_inscripcion_inicio__lte=today_min)
            & Q(fecha_inscripcion_final__gte=today_min))
        proyectos_en_proceso = Proyecto.objects.filter(
            Q(fecha_inicio__lte=today_min) & Q(fecha_final__gte=today_min))
        proyectos_realizados = Proyecto.objects.filter(
            fecha_final__lt=today_min)
        proyectos = {
            'inscripcion_activa': proyectos_inscripcion_activa,
            'proceso': proyectos_en_proceso,
            'realizados': proyectos_realizados,
        }
        kwargs['proyectos'] = proyectos
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active
        })
        return kwargs


class ProjectDetailView(LoginRequiredMixin, ProjectBase, DetailView):
    model = Proyecto
    template_name = 'proyecto/proyecto/detail.dj.html'
    slug_field = 'slug_generacion'
    slug_url_kwarg = 'slug_generacion'

    def menu(self):
        this_menu = super().menu()
        module = this_menu.get(self.app_active)
        kwargs = {"slug_generacion": self.object.slug_generacion}
        if self.member.has_perm('change_proyecto', self.object):
            update = LinkData("Editar",
                              "proyecto:project_edit",
                              opts={
                                  "kwargs": kwargs,
                              },
                              css_class="has-text-primary",
                              model="proyecto",
                              perms=["change"])
            module.append(update)
        if self.member.has_perm('delete_proyecto', self.object):
            delete = LinkData("Borrar",
                              "proyecto:project_delete",
                              opts={
                                  "kwargs": kwargs,
                              },
                              css_class="has-text-danger",
                              model="proyecto",
                              perms=["delete"])
            module = this_menu.get(self.app_active)
            module.append(delete)
        if self.object.group and self.member.has_perm('add_member',
                                                      self.object.group):
            add_member = LinkData(
                "Agregar Miembro",
                "admin_group:member2group_project",
                opts={"kwargs": {
                    "pk": self.object.group.id
                }},
                css_class="has-text-info")
            module.append(add_member)
        return this_menu

    def get_context_data(self, *args, **kwargs):
        user = self.request.user
        self.member = get_member(user)
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active,
            "main_group": self.object.group,
            "tipo": "project",
            "member": self.member
        })
        if self.object.group:
            kwargs.update({
                "members": {
                    "responsables":
                    self.object.group.member_creador_y_responsables(),
                    "profesores":
                    self.object.group.member_profesores(),
                    "ayudantes":
                    self.object.group.member_ayudantes(),
                    "estudiantes":
                    self.object.group.member_estudiantes(),
                    "postulantes": [],
                    "rechazados": [],
                },
                "groups": {
                    "responsables":
                    self.object.group.get_creador_y_responsables(),
                    "profesores": self.object.group.get_profesores(),
                    "ayudantes": self.object.group.get_ayudantes(),
                    "estudiantes": self.object.group.get_estudiantes(),
                },
            })
        return kwargs


class ProjectCourseGenerationView(ProjectBase, DetailView):
    model = Proyecto
    template_name = 'proyecto/proyecto/show.dj.html'
    slug_field = 'slug_generacion'
    slug_url_kwarg = 'slug_generacion'

    def menu(self):
        kwargs = {
            "slug_generacion": self.object.slug_generacion,
            "slug_curso": self.object.curso.slug_nombre
        }
        this_menu = {
            "Índice": [
                LinkData("El curso",
                         "proyecto:course_generation",
                         opts={"kwargs": kwargs},
                         go_to="sobre_curso"),
                LinkData("Fechas",
                         "proyecto:course_generation",
                         opts={"kwargs": kwargs},
                         go_to="fechas_importantes"),
                LinkData("Generación",
                         "proyecto:course_generation",
                         opts={"kwargs": kwargs},
                         go_to="generacion"),
                LinkData("Localización",
                         "proyecto:course_generation",
                         opts={"kwargs": kwargs},
                         go_to="localizacion"),
                LinkData("Quienes enseñan",
                         "proyecto:course_generation",
                         opts={"kwargs": kwargs},
                         go_to="profesores"),
                LinkData("Sesiones",
                         "proyecto:course_generation",
                         opts={"kwargs": kwargs},
                         go_to="sesiones"),
                LinkData("Valores",
                         "proyecto:course_generation",
                         opts={"kwargs": kwargs},
                         go_to="valores"),
                LinkData("Inscribirse",
                         "proyecto:course_generation",
                         opts={"kwargs": kwargs},
                         go_to="a_formulario"),
            ]
        }
        return this_menu

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        today = datetime.today().date()
        inscribirse = today <= self.object.fecha_inscripcion_final
        kwargs.update({
            'menu_nav': menu_nav,
            "app_active": self.app_active,
            "show_menu": self.menu(),
            "inscribirse": inscribirse,
            "today": today,
            "inscripcion_final": self.object.fecha_inscripcion_final
        })
        return kwargs


class ProjectCreateView(LoginRequiredMixin, ProjectBase, CreateView):
    model = Proyecto
    form_class = ProyectoForm
    template_name = 'proyecto/proyecto/create.dj.html'

    def get_initial(self):
        initial = super().get_initial()
        initial["usuario"] = self.request.user.id
        return initial

    def get_context_data(self, **kwargs):
        """
        Dado el nombre del proyecto en slug
        se consulta la información del proyecto
        para entregar la visualización relacionada a este mismo
        """
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active,
            "select_date": True
        })
        return kwargs


class ProjectEditView(ProjectBase, UpdateView):
    model = Proyecto
    form_class = ProyectoForm
    template_name = 'proyecto/proyecto/edit.dj.html'
    slug_field = 'slug_generacion'
    slug_url_kwarg = 'slug_generacion'

    def menu(self):
        this_menu = super().menu()
        kwargs = {"slug_generacion": self.object.slug_generacion}
        delete = LinkData("Borrar",
                          "proyecto:project_delete",
                          opts={
                              "kwargs": kwargs,
                          },
                          css_class="has-text-danger")
        module = this_menu.get(self.app_active)
        module.append(delete)
        return this_menu

    def get_context_data(self, **kwargs):
        """
        Dado el nombre del proyecto en slug
        se consulta la información del proyecto
        para entregar la visualización relacionada a este mismo
        """
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active,
            "select_date": True
        })
        return kwargs


class ProjectDeleteConfirmView(ProjectBase, DeleteView):
    model = Proyecto
    template_name = 'proyecto/proyecto/confirm_delete.dj.html'
    slug_field = 'slug_generacion'
    slug_url_kwarg = 'slug_generacion'

    def menu(self):
        this_menu = super().menu()
        kwargs = {"slug_generacion": self.object.slug_generacion}
        module = this_menu.get(self.app_active)
        if self.member.has_perm('change_proyecto', self.object):
            update = LinkData("Editar",
                              "proyecto:project_edit",
                              opts={
                                  "kwargs": kwargs,
                              },
                              css_class="has-text-primary")
            module.append(update)
        return this_menu

    def get_context_data(self, **kwargs):
        """
        Dado el nombre del proyecto en slug
        se consulta la información del proyecto
        para entregar la visualización relacionada a este mismo
        """
        user = self.request.user
        self.member = get_member(user)
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active,
            "member": self.member
        })
        return kwargs


"""
Clasification
"""


class ClasificationBase(LoginRequiredMixin):
    app_active = "Clasificación"

    def menu(self):
        this_menu = app_menu()
        return this_menu


class ClasificationListView(ClasificationBase, ListView):
    model = Clasificacion
    template_name = 'proyecto/clasificacion/list.dj.html'

    def get_context_data(self, **kwargs):
        """
        Dado el nombre del proyecto en slug
        se consulta la información del proyecto
        para entregar la visualización relacionada a este mismo
        """
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active
        })
        return kwargs


class ClasificationDetailView(ClasificationBase, DetailView):
    model = Clasificacion
    template_name = 'proyecto/clasificacion/detail.dj.html'
    slug_field = 'slug_tipo'
    slug_url_kwarg = 'slug_tipo'

    def menu(self):
        this_menu = super().menu()
        kwargs = {"slug_tipo": self.object.slug_tipo}
        clasification = this_menu.get(self.app_active)
        if self.member.has_perm('change_proyecto', self.object):
            update = LinkData("Editar",
                              "proyecto:clasification_edit",
                              opts={
                                  "kwargs": kwargs,
                              },
                              css_class="has-text-primary")
            clasification.append(update)
        if self.member.has_perm('delete_proyecto', self.object):
            delete = LinkData("Borrar",
                              "proyecto:clasification_delete",
                              opts={
                                  "kwargs": kwargs,
                              },
                              css_class="has-text-danger")
            clasification.append(delete)
        return this_menu

    def get_context_data(self, **kwargs):
        """
        Dado el nombre del proyecto en slug
        se consulta la información del proyecto
        para entregar la visualización relacionada a este mismo
        """
        user = self.request.user
        self.member = get_member(user)
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active
        })
        return kwargs


class ClasificationEditView(ClasificationBase, UpdateView):
    model = Clasificacion
    template_name = 'proyecto/clasificacion/edit.dj.html'
    fields = ["tipo", "descripcion"]
    slug_field = 'slug_tipo'
    slug_url_kwarg = 'slug_tipo'

    def menu(self):
        this_menu = super().menu()
        kwargs = {"slug_tipo": self.object.slug_tipo}
        delete = LinkData("Borrar",
                          "proyecto:clasification_delete",
                          opts={"kwargs": kwargs},
                          css_class="has-text-danger")
        clasification = this_menu.get(self.app_active)
        clasification.append(delete)
        return this_menu

    def get_context_data(self, **kwargs):
        """
        Dado el nombre del proyecto en slug
        se consulta la información del proyecto
        para entregar la visualización relacionada a este mismo
        """
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active
        })
        return kwargs


class ClasificationCreateView(ClasificationBase, CreateView):
    model = Clasificacion
    template_name = 'proyecto/clasificacion/create.dj.html'
    fields = ["tipo", "descripcion"]

    def get_context_data(self, **kwargs):
        """
        Dado el nombre del proyecto en slug
        se consulta la información del proyecto
        para entregar la visualización relacionada a este mismo
        """
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active
        })
        return kwargs


class ClasificationDeleteConfirmView(ClasificationBase, DeleteView):
    model = Clasificacion
    template_name = 'proyecto/clasificacion/confirm_delete.dj.html'
    slug_field = 'slug_tipo'
    slug_url_kwarg = 'slug_tipo'

    def menu(self):
        this_menu = super().menu()
        kwargs = {"slug_tipo": self.object.slug_tipo}
        update = LinkData("Editar",
                          "proyecto:clasification_edit",
                          opts={
                              "kwargs": kwargs,
                          },
                          css_class="has-text-primary")
        clasification = this_menu.get(self.app_active)
        clasification.append(update)
        return this_menu

    def get_context_data(self, **kwargs):
        """
        Dado el nombre del proyecto en slug
        se consulta la información del proyecto
        para entregar la visualización relacionada a este mismo
        """
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active,
        })
        return kwargs


"""
Course views
"""


class CourseBase(LoginRequiredMixin):
    app_active = "Curso"

    def menu(self):
        this_menu = app_menu()
        return this_menu


class CourseListView(CourseBase, ListView):
    template_name = 'proyecto/curso/list.dj.html'
    model = Curso

    def get_context_data(self, **kwargs):
        """
        Dado el nombre del proyecto en slug
        se consulta la información del proyecto
        para entregar la visualización relacionada a este mismo
        """
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active
        })
        return kwargs


class CourseDetailView(CourseBase, DetailView):
    template_name = 'proyecto/curso/detail.dj.html'
    model = Curso
    slug_field = 'slug_nombre'
    slug_url_kwarg = 'slug_nombre'

    def menu(self):
        this_menu = super().menu()
        kwargs = {"slug_nombre": self.object.slug_nombre}
        update = LinkData("Editar",
                          "proyecto:course_edit",
                          opts={
                              "kwargs": kwargs,
                          },
                          css_class="has-text-primary")
        delete = LinkData("Borrar",
                          "proyecto:course_delete",
                          opts={
                              "kwargs": kwargs,
                          },
                          css_class="has-text-danger")
        clasification = this_menu.get(self.app_active)
        clasification.append(update)
        clasification.append(delete)
        return this_menu

    def get_context_data(self, **kwargs):
        """
        Dado el nombre del proyecto en slug
        se consulta la información del proyecto
        para entregar la visualización relacionada a este mismo
        """
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active,
            "tipo": "course"
        })
        return kwargs


class CourseCreateView(CourseBase, CreateView):
    template_name = 'proyecto/curso/create.dj.html'
    form_class = CursoForm
    model = Curso

    def get_initial(self):
        initial = super().get_initial()
        initial["usuario"] = self.request.user.id
        return initial

    def get_context_data(self, **kwargs):
        """
        Dado el nombre del proyecto en slug
        se consulta la información del proyecto
        para entregar la visualización relacionada a este mismo
        """
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active
        })
        return kwargs


class CourseEditView(CourseBase, UpdateView):
    template_name = 'proyecto/curso/edit.dj.html'
    form_class = CursoForm
    model = Curso
    slug_field = 'slug_nombre'
    slug_url_kwarg = 'slug_nombre'

    def get_initial(self):
        initial = super().get_initial()
        initial["usuario"] = self.request.user.id
        return initial

    def get_context_data(self, **kwargs):
        """
        Dado el nombre del proyecto en slug
        se consulta la información del proyecto
        para entregar la visualización relacionada a este mismo
        """
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active
        })
        return kwargs


class CourseDeleteConfirmView(CourseBase, DeleteView):
    template_name = 'proyecto/proyecto/confirm_delete.dj.html'
    model = Curso
    slug_field = 'slug_nombre'
    slug_url_kwarg = 'slug_nombre'
    success_url = reverse_lazy("proyecto:course_list")

    def menu(self):
        this_menu = super().menu()
        kwargs = {"slug_nombre": self.object.slug_nombre}
        update = LinkData("Editar",
                          "proyecto:course_edit",
                          opts={
                              "kwargs": kwargs,
                          },
                          css_class="has-text-primary")
        module = this_menu.get(self.app_active)
        module.append(update)
        return this_menu

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active
        })
        return kwargs


"""
Value views
"""


class CourseValueBase(LoginRequiredMixin):
    app_active = "Valor"

    def menu(self):
        this_menu = app_menu()
        return this_menu


class CourseValueListView(CourseValueBase, ListView):
    template_name = 'proyecto/valor_curso/list.dj.html'
    model = ValorCurso

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active
        })

        return kwargs


class CourseValueDetailView(CourseValueBase, DetailView):
    template_name = 'proyecto/valor_curso/detail.dj.html'
    model = ValorCurso

    def menu(self):
        this_menu = super().menu()
        kwargs = {"pk": self.object.pk}
        update = LinkData("Editar",
                          "proyecto:coursevalue_edit",
                          opts={
                              "kwargs": kwargs,
                          },
                          css_class="has-text-primary")
        delete = LinkData("Borrar",
                          "proyecto:coursevalue_delete",
                          opts={
                              "kwargs": kwargs,
                          },
                          css_class="has-text-danger")
        value = this_menu.get(self.app_active)
        value.append(update)
        value.append(delete)
        return this_menu

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active
        })
        return kwargs


class CourseValueCreateView(CourseValueBase, CreateView):
    template_name = 'proyecto/valor_curso/create.dj.html'
    model = ValorCurso
    fields = ["precio", "asignacion"]

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active
        })
        return kwargs


class CourseValueEditView(CourseValueBase, UpdateView):
    template_name = 'proyecto/valor_curso/edit.dj.html'
    model = ValorCurso
    fields = ["precio", "asignacion"]

    def menu(self):
        this_menu = super().menu()
        kwargs = {"pk": self.object.pk}
        delete = LinkData("Borrar",
                          "proyecto:coursevalue_delete",
                          opts={
                              "kwargs": kwargs,
                          },
                          css_class="has-text-danger")
        value = this_menu.get(self.app_active)
        value.append(delete)
        return this_menu

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active
        })
        return kwargs


class CourseValueDeleteConfirmView(CourseValueBase, DeleteView):
    template_name = 'proyecto/valor_curso/confirm_delete.dj.html'
    model = ValorCurso
    success_url = reverse_lazy("proyecto:course_list")

    def menu(self):
        this_menu = super().menu()
        kwargs = {"pk": self.object.pk}
        update = LinkData("Editar",
                          "proyecto:coursevalue_edit",
                          opts={
                              "kwargs": kwargs,
                          },
                          css_class="has-text-primary")
        value = this_menu.get(self.app_active)
        value.append(update)
        return this_menu

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active
        })
        return kwargs


class EmailBase(LoginRequiredMixin):
    app_active = "Email"

    def menu(self):
        this_menu = app_menu()
        return this_menu

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "menu": self.menu(),
            "app_active": self.app_active,
            "ADVICE": EmailTemplates.ADVICE
        })
        return kwargs


class EmailsListView(EmailBase, ListView):
    template_name = "proyecto/emails/list.dj.html"
    model = EmailTemplates


class EmailsCreateView(EmailBase, CreateView):
    template_name = "proyecto/emails/create.dj.html"
    form_class = EmailTemplatesForm
    success_url = reverse_lazy("proyecto:emails_list")


class EmailsEditView(EmailBase, UpdateView):
    template_name = "proyecto/emails/edit.dj.html"
    form_class = EmailTemplatesForm
    model = EmailTemplates
    success_url = reverse_lazy("proyecto:emails_list")


class EmailsDetailView(EmailBase, DetailView):
    template_name = "proyecto/emails/detail.dj.html"
    model = EmailTemplates


class EmailsDeleteConfirmView(EmailBase, DeleteView):
    template_name = "proyecto/emails/delete_confirm.dj.html"
    model = EmailTemplates


class EmailsPreviewView(EmailBase, DetailView):
    template_name = "proyecto/emails/preview.dj.html"
    model = EmailTemplates

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        field = self.kwargs.get("field")
        fn = getattr(self.object, f"load_template_{field}", None)
        if fn:
            context["email_html"] = fn()
        return context


def menu_cursos():
    menu = {"Actuales": [], "Pasados": [], "Postular": []}
    return menu


class BaseMisCursos(LoginRequiredMixin):
    app_active = 'Mis Cursos'

    def menu(self):
        return menu_cursos()


class MisCursos(BaseMisCursos, TemplateView):
    model = User
    template_name = 'proyecto/mis_cursos.dj.html'
    pk_url_kwarg = 'username'
    app_active = 'Mis Cursos'

    def menu(self):
        this_menu = {
            "Contenidos": [LinkData("Aplicación", "contenido:application")],
            "Actuales": [],
            "Pasados": [],
            "Inscritos": [],
            "Resúmenes": []
        }
        return this_menu

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs["menu_title"] = self.app_active
        kwargs.update({
            'menu_nav': menu_nav,
            "app_active": self.app_active,
            "menu": self.menu
        })
        user = self.request.user
        member = user.groups_manager_member_set.first()
        kwargs["administrar"] = my_proyectos(member)
        kwargs["cursos"] = my_proyectos(member, managers=["Estudiante"])
        return kwargs
