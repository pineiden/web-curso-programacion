import time
from django.dispatch import receiver
from groups_manager.models import (Group, Member, GroupEntity, GroupMember,
                                   GroupMemberRole)
from groups_manager.models import group_save, group_delete
from django.db.models.signals import post_save, post_delete
from files.functions.diccionarios.ops import union_complemento
from groups_manager.models import Group, GroupType
from apps.propuesta.models import Propuesta
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.template import Template, Context
import math
from markdownx.models import MarkdownxField
from django.db import models
from django.utils.text import slugify
import ujson as json
import functools as fc
from datetime import datetime
from django.urls import reverse
from files.functions.dolar_today import get_dolar2pesos
from networktools.library import my_random_string
from django.db.models.signals import pre_save
try:
    CAMBIO_USD = get_dolar2pesos()
except:
    CAMBIO_USD = 770


def get_upload_pagina_file_name(field, instance, filename):
    if field in {'proyecto', 'icon_totem', 'img_totem', 'afiche', 'fondo'}:
        print("Campo a generar, %s" % field)
        return "proyecto/%s/%s/%s" % (field, instance.slug_generacion,
                                      filename)
    elif field == 'curso':
        return "proyecto/%s/%s/%s" % (field, instance.slug_nombre, filename)


class Clasificacion(models.Model):
    tipo = models.CharField(max_length=150, unique=True)
    slug_tipo = models.SlugField(default=None, blank=True)
    descripcion = models.TextField(blank=True)

    def get_absolute_url(self):
        url = reverse('proyecto:clasification_detail',
                      kwargs={'slug_tipo': self.slug_tipo})
        print(f"La url success {url}")
        return url

    def save(self, *args, **kwargs):
        self.slug_tipo = slugify(self.tipo)
        # TODO: check if exists before save
        instance = super().save(*args, **kwargs)
        return instance

    @property
    def Clasificacion(self):
        return self.tipo

    class Meta:
        app_label = "proyecto"
        verbose_name = "Clasificación"
        verbose_name_plural = "Clasificaciones"
        ordering = ("tipo", )

    def __repr__(self):
        return json.dumps(self.__dict__)

    def __str__(self):
        return self.tipo


"""
Group Manager Classes
"""


class GrupoCurso(Group):
    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        if not self.group_type:
            self.group_type = GroupType.objects.get_or_create(label='Curso')[0]
        super().save(*args, **kwargs)

    def get_sub_grupo(self, name):
        return self.sub_groups_manager_group_set.get(name=name)

    def get_sub_grupos(self, member):
        sub_groups = self.sub_grupos()
        member_groups = []
        for sb in sub_groups:
            qs = sb.group_membership.filter(member=member)
            if qs:
                member_groups.append(sb)
        return member_groups

    def get_members(self, subgroups=True):
        return super().get_members(subgroups=subgroups)

    def get_roles(self):
        group_members = self.group_membership.all()
        roles = GroupMemberRole.objects.none()
        for gm in group_members:
            roles |= gm.roles.all()
        return roles.distinct()

    def sub_grupos(self):
        return list(
            self.sub_groups_manager_group_set.filter(
                name__in=["Creador", "Responsable"]))

    def sub_proyectos(self):
        return list(self.sub_groups_manager_group_set.all().exclude(
            name__in=["Creador", "Responsable"]))

    def get_creadores(self):
        return list(self.sub_groups_manager_group_set.filter(name="Creador"))

    def get_responsables(self):
        return list(
            self.sub_groups_manager_group_set.filter(name="Responsable"))


class GrupoProyecto(Group):
    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        if not self.group_type:
            self.group_type = GroupType.objects.get_or_create(
                label='Proyecto')[0]
        super().save(*args, **kwargs)

    def get_members_dict(self, subgroups=True):
        members = {}
        for group in self.sub_grupos():
            members[group.name] = group.get_members()
        return members

    def get_sub_grupos(self, member):
        sub_groups = self.sub_grupos()
        member_groups = []
        for sb in sub_groups:
            qs = sb.group_membership.filter(member=member)
            if qs:
                member_groups.append(sb)
        return member_groups

    def sub_grupos(
        self,
        names=["Creador", "Responsable", "Profesor", "Ayudante",
               "Estudiante"]):
        return list(self.sub_groups_manager_group_set.filter(name__in=names))

    def get_roles(self):
        group_members = self.group_membership.filter(group=self)
        roles = GroupMemberRole.objects.none()
        for gm in group_members:
            roles |= gm.roles.all()
        return roles.distinct()

    def get_creadores(self):
        """
        Creator group
        """
        return list(self.sub_groups_manager_group_set.filter(name="Creador"))

    def member_creadores(self):
        creadores = self.get_creadores()
        members = set()
        for group in creadores:
            group_members = group.group_membership.all()
            for gmember in group_members:
                members.add(gmember.member)
        return set(members)

    def get_responsables(self):
        return list(
            self.sub_groups_manager_group_set.filter(name="Responsable"))

    def member_responsables(self):
        creadores = self.get_responsables()
        members = set()
        for group in creadores:
            group_members = group.group_membership.all()
            for gmember in group_members:
                members.add(gmember.member)
        return set(members)

    def get_creador_y_responsables(self):
        return list(
            self.sub_groups_manager_group_set.filter(
                name__in=["Creador", "Responsable"]))

    def member_creador_y_responsables(self):
        creadores = self.get_creador_y_responsables()
        members = set()
        for group in creadores:
            group_members = group.group_membership.all()
            for gmember in group_members:
                members.add(gmember.member)
        return set(members)

    def get_profesores(self):
        return list(self.sub_groups_manager_group_set.filter(name="Profesor"))

    def member_profesores(self):
        creadores = self.get_profesores()
        members = set()
        for group in creadores:
            group_members = group.group_membership.all()
            for gmember in group_members:
                members.add(gmember.member)
        return set(members)

    def get_ayudantes(self):
        return list(self.sub_groups_manager_group_set.filter(name="Ayudante"))

    def member_ayudantes(self):
        creadores = self.get_ayudantes()
        members = set()
        for group in creadores:
            group_members = group.group_membership.all()
            for gmember in group_members:
                members.add(gmember.member)
        return set(members)

    def get_estudiantes(self):
        return list(
            self.sub_groups_manager_group_set.filter(name="Estudiante"))

    def member_estudiantes(self):
        creadores = self.get_estudiantes()
        members = set()
        for group in creadores:
            group_members = group.group_membership.all()
            for gmember in group_members:
                members.add(gmember.member)
        return set(members)


class Curso(models.Model):
    nombre = models.CharField(default='Nombre de Curso',
                              max_length=200,
                              unique=True)
    slug_nombre = models.SlugField(blank=True, max_length=200)
    imagen = models.ImageField(upload_to=fc.partial(
        get_upload_pagina_file_name, 'curso'),
        max_length=700)
    resumen = models.TextField(default="Resumen de Curso")
    descripcion = models.TextField()
    clasificacion = models.ManyToManyField(Clasificacion,
                                           related_name="courses")
    date_time = models.DateTimeField(auto_now=True)
    usuario = models.ForeignKey(User,
                                related_name="cursos",
                                on_delete=models.SET_NULL,
                                blank=True,
                                null=True)
    propuesta = models.ForeignKey(Propuesta,
                                  related_name="cursos",
                                  on_delete=models.SET_NULL,
                                  blank=True,
                                  null=True)
    group = models.OneToOneField(GrupoCurso,
                                 related_name='curso',
                                 on_delete=models.SET_NULL,
                                 blank=True,
                                 null=True)

    @classmethod
    def create_unique_slug(cls, item):
        slug_nombre = slugify(item.nombre)
        items = cls.objects.filter(slug_nombre=slug_nombre)
        while items:
            append = my_random_string(3).lower()
            slug_nombre = f"{slug_nombre}_{append}"
            items = cls.objects.filter(slug_nombre=slug_nombre)
        return slug_nombre

    def save(self, *args, **kwargs):
        pre = Curso.objects.filter(id=self.id).first()
        if not self.slug_nombre:
            if pre:
                if self.nombre != pre.nombre:
                    self.slug_nombre = self.create_unique_slug(self)
            else:
                self.slug_nombre = self.create_unique_slug(self)
        crea_grupo_curso(self)
        return super().save(*args, **kwargs)

    @property
    def slug(self):
        return self.slug_nombre

    def get_absolute_url(self):
        return reverse('proyecto:course_detail',
                       kwargs={'slug_nombre': self.slug_nombre})

    @property
    def Curso(self):
        return self.tipo

    class Meta:
        app_label = "proyecto"
        verbose_name = "Curso"
        verbose_name_plural = "Cursos"
        ordering = ("nombre", )
        permissions = (("create_course", "Crear Curso"), )

    def __repr__(self):
        return json.dumps(self.__dict__)

    def __str__(self):
        return self.nombre


class ValorCurso(models.Model):
    precio = models.IntegerField()
    asignacion = models.CharField(max_length=100)

    def get_absolute_url(self):
        return reverse('proyecto:coursevalue_detail', kwargs={'pk': self.pk})

    @property
    def ValorCurso(self):
        return self.precio

    @property
    def usd(self):
        return math.ceil(self.precio / CAMBIO_USD)

    class Meta:
        app_label = "proyecto"
        verbose_name = "Valor"
        verbose_name_plural = "Valores"
        ordering = ("precio", )

    def __repr__(self):
        return json.dumps(self.__dict__)

    def __str__(self):
        return self.asignacion


class ProyectoManager(models.Manager):
    def get_by_natural_key(self, slug_generacion):
        return self.get(slug_generacion=slug_generacion)


class Proyecto(models.Model):
    curso = models.ForeignKey(Curso,
                              on_delete=models.SET_NULL,
                              null=True,
                              default=1)
    valor = models.ManyToManyField(ValorCurso, related_name="projects")
    generacion = models.CharField(default='Animal', max_length=100)
    slug_generacion = models.SlugField(unique=True, blank=True)
    imagen_totem = models.ImageField(upload_to=fc.partial(
        get_upload_pagina_file_name, 'img_totem'),
        default='proyecto/piel.png')
    icono_totem = models.ImageField(upload_to=fc.partial(
        get_upload_pagina_file_name, 'icon_totem'),
        default='proyecto/piel.png')
    afiche = models.ImageField(upload_to=fc.partial(
        get_upload_pagina_file_name, 'afiche'),
        default='proyecto/piel.png')
    imagen_fondo = models.ImageField(upload_to=fc.partial(
        get_upload_pagina_file_name, 'fondo'),
        default='proyecto/piel.png')
    resumen = models.TextField(default="Resumen de Proyecto")
    descripcion = models.TextField()
    fecha_inscripcion_inicio = models.DateField()
    fecha_inscripcion_final = models.DateField()
    # generar suscripcion a un calendario
    # que envie correos
    fecha_inicio = models.DateField()
    fecha_final = models.DateField()
    usuario = models.ForeignKey(User,
                                related_name="proyectos",
                                on_delete=models.SET_NULL,
                                null=True,
                                blank=True)
    group = models.OneToOneField(GrupoProyecto,
                                 related_name='proyecto',
                                 on_delete=models.SET_NULL,
                                 blank=True,
                                 null=True)

    objects = ProyectoManager()

    def natural_key(self):
        return (self.slug_generacion, )

    def get_absolute_url(self):
        if self.curso:
            return reverse('proyecto:project_detail',
                           kwargs={
                               'slug_generacion': self.slug_generacion,
                               'slug_curso': self.curso.slug_nombre
                           })
        else:
            return reverse("proyecto:project_list")

    @classmethod
    def create_unique_slug(cls, item):
        slug_nombre = slugify(item.generacion)
        items = cls.objects.filter(slug_generacion=slug_nombre)
        while items:
            append = my_random_string(3).lower()
            slug_nombre = f"{slug_nombre}_{append}"
            items = cls.objects.filter(slug_generacion=slug_nombre)
        return slug_nombre

    def save(self, *args, **kwargs):
        pre = Proyecto.objects.filter(id=self.id).first()
        if not self.slug_generacion:
            if pre:
                if self.generacion != pre.generacion:
                    self.slug_generacion = self.create_unique_slug(self)
            else:
                self.slug_generacion = self.create_unique_slug(self)
        crea_grupo_proyecto(self)
        super().save(*args, **kwargs)
        email_tpl, created = EmailTemplates.objects.get_or_create(
            proyecto=self)
        email_tpl.save()

    @property
    def slug(self):
        return self.slug_generacion

    @property
    def Clasificacion(self):
        return self.tipo

    @property
    def ver_mas(self):
        ver_mas = "Ver curso e inscribirse"
        today = datetime.now().date()
        if today > self.fecha_inscripcion_inicio:
            return "Ver curso, sin inscripción"
        if today >= self.fecha_inicio and today <= self.fecha_final:
            return "Ver curso, dictándose actualmente"
        else:
            return ver_mas

    @property
    def ver_mas_url(self):
        ver_mas = reverse("home:index")
        today = datetime.now().date()
        kwargs_url = {
            "slug_generacion": self.slug_generacion,
            "slug_curso": self.curso.slug_nombre
        }
        if today > self.fecha_inscripcion_final:
            return reverse("proyecto:proyecto", kwargs=kwargs_url)
        if today >= self.fecha_inscripcion_inicio and today <= self.fecha_inscripcion_final:
            return reverse("inscripcion:persona", kwargs=kwargs_url)
        else:
            return ver_mas

    @property
    def share_data(self):
        long_text_tpl = "Te invito a revisar el nuevo curso de %s, generación %s, será desde el día %s"
        share = {
            'url':
            self.get_absolute_url(),
            'long_text':
            long_text_tpl %
            (self.curso.nombre, self.generacion, self.fecha_inicio),
            'subject':
            "Nuevo curso de %s" % self.curso.nombre
        }
        return share

    class Meta:
        app_label = "proyecto"
        verbose_name = "Proyecto"
        verbose_name_plural = "Proyectos"
        ordering = ("generacion", )
        permissions = (("create_project", "Crear Proyecto"), )

    def __repr__(self):
        return json.dumps({
            'curso': self.curso.nombre,
            'generacion': self.generacion
        })

    def __str__(self):
        return f"Generación '{self.generacion}'"


class EmailTemplates(models.Model):
    PRE_ = """
    {% load inlinecss %}
    {% inlinecss 'inscripcion/css/email.css %}
    <html>
    <body>
    <article>
    """
    _POS = """
    </article>
    </body>
    </html>
    """
    ADVICE = """
    <div class="section">
    Se recomienda utilizar etiquetas HTML.
    Las variables posibles a usar disponibles son:
    Para Usuario
    <ul>
    <li>{{nombre}} :: nombre de curso y generacion</li>
    <li>{{nombre_completo}} :: nombre de la persona</li>
    <li>{{fecha_inicio}} :: fecha inicio curso</li>
    <li>{{fecha_final}} :: fecha final curso</li>
    </ul>
    Para administradores:
    <li>{{nombre}} :: nombre de curso y generación</li>
    <li>{{data_persona.username}} :: nombre de usuario</li>
    <li>{{data_persona.nombre_completo}} :: nombre completo</li>
    <li>{{data_persona.email}} :: correo electrónico de usuario</li>
    <li>{{data_persona.phonenumber}} :: teléfono</li>
    </div>

    <h4 class="title is-4">Guía para parámetros</h4>
    <p>El campo "parámetros" permite definir un diccionario json para hacer
    más flexible el uso de los parámetros o extender los parámetros
    disponibles.
    Cada campo principal corresponde al
    uso que se le dará a cada template en particular.</p>
    <p>Una vez que asignas valores a cada parámetro, podrás utilizarlo en el
    template y visualizar sus valores</p>
    <p>También, al finalizar la edicición, se recomienda enviar emails de
    prueba al usuario editor con el fin de chequear como resulta su template</p>
    """
    DEFAULT = "<div>Inscripción exitosa de {{nombre_completo}}</div>"
    DEFAULT_RECHAZO = """<h2>Estimad@ {{nombre_completo}}</h2>
    <p>Tenemos que comunicarte que lamentablemente tu inscripción a {{self.proyecto.curso}} ha sido rechazada
    <p>Porque:
    <p>{{mensaje_particular}}
    """
    DEFAULT_ACEPTA = """<h2>Tu inscripción a {{self.proyecto.curso}} ha sido aceptada</h2>
    <p>Las clases serán los días {{primer_dia}} y {{segundo_dia}} de cada semana.
    <p>Además podrás seguir cada clase en la web del curso en {{web}} y compartir
    <p>tus dudas y experiencias en el canal de {{chat}}
    <p>{{mensaje_particular}}
    """
    DEFAULT_ADMINS_RESULT = """
    El/la usuario {{revision.usario}} ha revisado de la inscripcion de {{inscripcion.usuario}}.
    El resultado es {{inscripcion.status}}.
    """

    def DEFAULT_JSON():
        return dict(exitosa_usuario=dict(),
                    exitosa_admin=dict(),
                    rechazo=dict(),
                    acepta=dict(),
                    result_admins=dict())

    proyecto = models.OneToOneField(Proyecto,
                                    related_name="email_templates",
                                    on_delete=models.CASCADE)
    inscripcion_exitosa_usuario = models.TextField(default=DEFAULT)
    inscripcion_exitosa_admin = models.TextField(default=DEFAULT)
    inscripcion_rechazo = models.TextField(default=DEFAULT_RECHAZO)
    inscripcion_acepta = models.TextField(default=DEFAULT_ACEPTA)
    inscripcion_result_admins = models.TextField(default=DEFAULT_ADMINS_RESULT)
    parametros = JSONField(default=DEFAULT_JSON)
    date_time = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = "proyecto"
        verbose_name = "Email Templates"
        verbose_name_plural = "Emails Templates"
        ordering = ("date_time", )

    def load_template(self, field, **context):
        text = f"{self.PRE_}{field}{self._POS}"
        template = Template(field)
        context = Context(context)
        return template.render(context)

    def load_template_admins(self, **context):
        data = union_complemento(self.parametros.get("exitosa_admin", {}),
                                 context)
        return self.load_template(self.inscripcion_exitosa_admin, **data)

    def load_template_user(self, **context):
        data = union_complemento(self.parametros.get("exitosa_user", {}),
                                 context)
        return self.load_template(self.inscripcion_exitosa_usuario, **data)

    def load_template_acepta(self, **context):
        data = union_complemento(self.parametros.get("acepta", {}), context)
        return self.load_template(self.inscripcion_acepta, **data)

    load_template_accepted = load_template_acepta

    def load_template_rechazo(self, **context):
        data = union_complemento(self.parametros.get("rechaza", {}), context)
        return self.load_template(self.inscripcion_rechazo, **data)

    load_template_rejected = load_template_rechazo

    def load_template_result_admins(self, **context):
        data = union_complemento(self.parametros.get("result_admins", {}),
                                 context)
        return self.load_template(self.inscripcion_result_admins, **data)


post_save.connect(group_save, sender=GrupoCurso)
post_delete.connect(group_delete, sender=GrupoCurso)
post_save.connect(group_save, sender=GrupoProyecto)
post_delete.connect(group_delete, sender=GrupoProyecto)


def get_member(user):
    profile = user.profile
    member, created = Member.objects.get_or_create(django_user=user)
    if created:
        member.username = user.username
        member.first_name = user.first_name
        member.last_name = f"{profile.father_last_name} {profile.mother_last_name}"
        member.email = user.email
        member.save()
    return member


def create_perms(group, subgroup, role):
    """
    * Curso:

    Rol Creador
    Rol Responsable

    * Proyecto

    Rol Creador
    Rol Responsable
    Rol Profesor
    Rol Ayudante
    Rol Estudiante

    """
    gtype = subgroup.group_type
    key = f"{group.group_type.label.lower()}_{role.lower()}"
    permissions_dict = {
        "curso_creador": {
            "owner": {
                "curso": ["change", "delete"],
                "default": ["view"]
            },
            "group": {
                "curso": [
                    "change",
                ],
                "default": ["view"]
            },
            "groups_upstream": {
                "curso": ["change", "delete"],
                "default": ["view"]
            },
            "groups_downstream": {
                "curso": [],
                "default": ["view"]
            },
            "groups_siblings": {
                "curso": [],
                "default": ["view"]
            },
        },
        "curso_responsable": {
            "owner": {
                "curso": [
                    "change",
                ],
                "default": ["view"]
            },
            "group": {
                "curso": [
                    "change",
                ],
                "default": ["view"]
            },
            "groups_upstream": {
                "curso": [
                    "change",
                ],
                "default": ["view"]
            },
            "groups_downstream": {
                "curso": [],
                "default": ["view"]
            },
            "groups_siblings": {
                "curso": [],
                "default": ["view"]
            },
        },
        "proyecto_creador": {
            "owner": {
                "proyecto": ["change", "delete"],
                "default": ["view"]
            },
            "group": {
                "proyecto": [
                    "change",
                ],
                "default": ["view"]
            },
            "groups_upstream": {
                "proyecto": ["change", "delete"],
                "default": ["view"]
            },
            "groups_downstream": {
                "proyecto": [],
                "default": ["view"]
            },
            "groups_siblings": {
                "proyecto": [],
                "default": ["view"]
            },
        },
        "proyecto_responsable": {
            "owner": {
                "proyecto": [
                    "change",
                ],
                "default": ["view"]
            },
            "group": {
                "proyecto": [
                    "change",
                ],
                "default": ["view"]
            },
            "groups_upstream": {
                "proyecto": [
                    "change",
                ],
                "default": ["view"]
            },
            "groups_downstream": {
                "proyecto": [],
                "default": ["view"]
            },
            "groups_siblings": {
                "proyecto": [],
                "default": ["view"]
            },
        },
        "proyecto_profesor": {
            "owner": {
                "proyecto": [
                    "change",
                ],
                "default": ["view"]
            },
            "group": {
                "proyecto": [
                    "change",
                ],
                "default": ["view"]
            },
            "groups_upstream": {
                "proyecto": [
                    "change",
                ],
                "default": ["view"]
            },
            "groups_downstream": {
                "proyecto": ["view"],
                "default": ["view"]
            },
            "groups_siblings": {
                "proyecto": ["view"],
                "default": ["view"]
            },
        },
        "proyecto_ayudante": {
            "owner": {
                "proyecto": [
                    "change",
                ],
                "default": ["view"]
            },
            "group": {
                "proyecto": [
                    "change",
                ],
                "default": ["view"]
            },
            "groups_upstream": {
                "proyecto": [
                    "view",
                ],
                "default": ["view"]
            },
            "groups_downstream": {
                "proyecto": ["view"],
                "default": ["view"]
            },
            "groups_siblings": {
                "proyecto": [],
                "default": ["view"]
            },
        },
        "proyecto_estudiante": {
            "owner": {
                "proyecto": [
                    "view",
                ],
                "default": ["view"]
            },
            "group": {
                "proyecto": [
                    "view",
                ],
                "default": ["view"]
            },
            "groups_upstream": {
                "proyecto": ["view"],
                "default": ["view"]
            },
            "groups_downstream": {
                "proyecto": ["view"],
                "default": ["view"]
            },
            "groups_siblings": {
                "proyecto": ["view"],
                "default": ["view"]
            },
        },
    }
    permissions_0 = permissions_dict.get(key, {})
    try:
        if subgroup not in group.sub_grupos():
            group.assign_object(subgroup, custom_permissions=permissions_0)
        return True
    except Exception as e:
        raise e


def init_entities(grupo):
    # his objects could describe the group's properties
    semilla_curso, c_0 = GroupEntity.objects.get_or_create(label="Semilla")
    if c_0:
        semilla_curso.save()
    responsable_curso, c_1 = GroupEntity.objects.get_or_create(
        label="Responsable")
    if c_1:
        responsable_curso.save()
    grupo.group_entities.add(semilla_curso)
    grupo.group_entities.add(responsable_curso)


def crea_grupo_curso(instance):
    base_curso, created = GrupoCurso.objects.get_or_create(name="Curso Base")
    if created:
        base_curso.save()
    name = instance.nombre
    owner = instance.usuario
    member = get_member(owner)
    grupo, created = GrupoCurso.objects.get_or_create(name=name,
                                                      parent=base_curso)
    if created:
        creador, nuevo = GroupType.objects.get_or_create(label='Creador')
        responsable, nuevo = GroupType.objects.get_or_create(
            label='Responsable')
        creador_rol, nuevo = GroupMemberRole.objects.get_or_create(
            label='Creador')
        responsable_rol, nuevo = GroupMemberRole.objects.get_or_create(
            label='Responsable')

        # Crear dos grupos que sean hijos de grupocurso
        # Crear Roles.
        grupo_creador, creado = Group.objects.get_or_create(name="Creador",
                                                            group_type=creador,
                                                            parent=grupo)
        grupo_creador.add_member(member, roles=[creador_rol])
        # grupo_creador.save()
        create_perms(grupo, grupo_creador, "creador")
        # grupo responsable
        grupo_responsable, creado = Group.objects.get_or_create(
            name="Responsable",
            parent=grupo,
            group_type=responsable,
        )
        grupo_responsable.add_member(member, roles=[responsable_rol])
        # grupo_responsable.save()
        create_perms(grupo, grupo_responsable, "responsable")
        # his objects could describe the group's properties
        # init_entities(grupo)
        # add_member -> crear Member, GroupMemberRole (roles), Group
        grupo.save()
        instance.group = grupo


def crea_grupo_proyecto(instance):
    name = instance.generacion
    owner = instance.usuario
    member = get_member(owner)
    curso = instance.curso
    grupo_curso = GrupoCurso.objects.get(name=curso.nombre)
    grupo, created = GrupoProyecto.objects.get_or_create(name=name,
                                                         parent=grupo_curso)
    if created:
        names = [
            "Creador", "Responsable", "Profesor", "Ayudante", "Estudiante"
        ]
        for name in names:
            print("Name->", name)
            element, new = GroupMemberRole.objects.get_or_create(label=name)
            gtype, nuevo = GroupType.objects.get_or_create(label=name)
            if new:
                element.save()
            group, created = Group.objects.get_or_create(name=name,
                                                         group_type=gtype,
                                                         parent=grupo)
            if name in ["Creador", "Responsable"] and not created:
                members = [gm.member for gm in group.group_membership.all()]
                if member not in members:
                    group.add_member(member, roles=[element])
                    group.save()
            create_perms(grupo, group, name)
        # Crear Roles.
        grupo.save()
        instance = group = grupo
