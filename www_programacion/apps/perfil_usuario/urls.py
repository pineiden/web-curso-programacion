from django.urls import path, re_path
# from django.contrib import admin
from apps.perfil_usuario import views

app_name = 'perfil_usuario'

urlpatterns = [
    path('usuario=<slug:username>',
         views.PerfilUsuario.as_view(),
         name='usuario'),
]
