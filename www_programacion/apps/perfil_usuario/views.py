from django.shortcuts import render
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import TemplateView

# Create your views here.

class PerfilUsuario(TemplateView):
    template_name = 'perfil_usuario/principal.dj.html'
