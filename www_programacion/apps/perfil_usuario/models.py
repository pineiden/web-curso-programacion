from django.db import models
from apps.accounts.models import Profile
from apps.proyecto.models import Proyecto

# Create your models here.


class AccesoUsuario(models.Model):
    """
    Por cada proyecto al que un usuario se suscribe
    se asigna el tipo de acceso que tendrá
    """
    OPCIONES = [
        ('org', 'Organizador'),
        ('prf', 'Profesor@'),
        ('est', 'Estudiante'),
        ('ay', 'Ayudante'),
        ('esc', 'Oyente'),
    ]
    usuario = models.ForeignKey(Profile, on_delete=models.SET('Borrad@'))
    proyecto = models.ForeignKey(Proyecto,
                                 on_delete=models.SET('Proyecto Eliminado'))
    tipo = models.CharField(max_length=10, choices=OPCIONES, default='esc')


def archivo_mensaje(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'mensajes/usuario_{0}/mensaje_{1}/filename_{2}'.format(
        instance.mensaje.usuario.id, instance.mensaje.id, filename)


class MensajeUsuario(models.Model):
    usuario = models.ForeignKey(Profile, on_delete=models.SET('Borrad@'))
    tema = models.CharField(max_length=200)
    texto = models.TextField()


class AdjuntoMensaje(models.Model):
    mensaje = models.ForeignKey(MensajeUsuario, on_delete=models.CASCADE)
    archivo = models.FileField(upload_to=archivo_mensaje)
