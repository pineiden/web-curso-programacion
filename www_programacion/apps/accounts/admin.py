from django.contrib import admin
from .models import Profile, ProfileImage, PrincipalImageProfile


# Register your models here.
@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = [
        "user", "first_name", "father_last_name", "mother_last_name", "email"
    ]


@admin.register(ProfileImage)
class ProfileImageAdmin(admin.ModelAdmin):
    list_display = ["id", "date_time", "image"]


@admin.register(PrincipalImageProfile)
class PrincipalImageProfileAdmin(admin.ModelAdmin):
    list_display = ["profile", "image", "date_time"]
