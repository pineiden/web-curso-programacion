from django.conf.urls import url
from django.urls import path
# from django.contrib import admin
from apps.home import views

app_name = 'home'

urlpatterns = [
    path('', views.HomeView.as_view(), name='index'),
    path('nosotros', views.NosotrosView.as_view(), name='nosotros'),
    #path('proyecto', views.ProyectoView.as_view(), name='proyecto'),
    #path('contacto', views.ContactoView.as_view(), name='contacto'),
]
