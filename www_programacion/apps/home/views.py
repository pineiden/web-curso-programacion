from django.views.generic import TemplateView
from django.shortcuts import render

# Create your views here.
from files.functions.links import LinkData
from apps.proyecto.functions import (submit_active_projects,
                                     happening_active_projects)


def noticias():
    return []


def menu_nav(**kwargs):
    navbar_css = "navbar-item"
    main_menu = [
        LinkData("Inicio",
                 "home:index",
                 css_class=f"{navbar_css} navbar-start"),
        LinkData("Nosotros",
                 "pagina:detalle_pagina",
                 opts={'kwargs': {
                     "slug_titulo": "nosotros"
                 }},
                 css_class=navbar_css),
        LinkData("Proyectos",
                 "pagina:detalle_pagina",
                 opts={'kwargs': {
                     "slug_titulo": "proyectos"
                 }},
                 css_class=navbar_css),
        LinkData("Inscribirse", "inscripcion:activas", css_class=navbar_css),
        LinkData("Historia",
                 "pagina:detalle_pagina",
                 opts={'kwargs': {
                     "slug_titulo": "historia"
                 }},
                 css_class=navbar_css),
        LinkData("Contacto",
                 "contacto:formulario",
                 css_class=f"{navbar_css} navbar-end"),
    ]

    return main_menu


def inicio_view(request):
    return render(request, 'home/home.dj.html', {})


class HomeView(TemplateView):
    template_name = 'home/home.dj.html'

    def get_context_data(self, *args, **kwargs):
        """
        Dado el nombre del proyecto en slug
        se consulta la información del proyecto
        para entregar la visualización relacionada a este mismo
        """
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({'menu_nav': menu_nav()})
        kwargs["inscripciones_activas"] = submit_active_projects()
        kwargs["cursos_activos"] = happening_active_projects()
        kwargs["cursos_realizados"] = happening_active_projects()
        kwargs["noticias"] = noticias()
        return kwargs


class NosotrosView(TemplateView):
    template_name = 'home/nosotros.dj.html'

    def get_context_data(self, *args, **kwargs):
        """
        Dado el nombre del proyecto en slug
        se consulta la información del proyecto
        para entregar la visualización relacionada a este mismo
        """
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({'menu_nav': menu_nav()})
        return kwargs


class ProyectoView(TemplateView):
    template_name = 'home/proyecto.dj.html'

    def get_context_data(self, *args, **kwargs):
        """
        Dado el nombre del proyecto en slug
        se consulta la información del proyecto
        para entregar la visualización relacionada a este mismo
        """
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({'menu_nav': menu_nav()})
        return kwargs


class ContactoView(TemplateView):
    template_name = 'home/contacto.dj.html'

    def get_context_data(self, *args, **kwargs):
        """
        Dado el nombre del proyecto en slug
        se consulta la información del proyecto
        para entregar la visualización relacionada a este mismo
        """
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({'menu_nav': menu_nav()})
        return kwargs


def bad_request(request, *args, **argv):
    response = render(request, '400.dj.html')
    response.status_code = 400
    return response


def permission_denied(request, *args, **argv):
    response = render(request, '403.dj.html')
    response.status_code = 403
    return response


def page_not_found(request, *args, **argv):
    response = render(request, '404.dj.html')
    response.status_code = 404
    return response


def server_error(request, *args, **argv):
    response = render(request, '500.dj.html')
    response.status_code = 500
    return response
