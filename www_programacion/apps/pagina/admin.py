from django.contrib import admin
from apps.pagina.models import (Clasificacion, Pagina)
# Register your models here.

admin.site.register(Clasificacion)
admin.site.register(Pagina)
