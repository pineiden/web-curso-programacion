from django.apps import AppConfig


class MensajeUsuarioConfig(AppConfig):
    name = 'mensaje_usuario'
