from django.db import models
from django.contrib.auth.models import User

# Create your models here.


def archivo_mensaje(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'mensajes/usuario_{0}/mensaje_{1}/filename'.format(
        instance.mensaje.usuario.id, instance.mensaje.id, filename)


class MensajeUsuario(models.Model):
    respuesta_a = models.ForeigKey(MensajeUsuario,
                                   on_delete=models.SET('Mensaje Borrado'))
    usuario = models.ForeignKey(User, on_delete=models.SET('Borrad@'))
    tema = models.CharField(max_length=200)
    texto = models.TextField()


class ArchivoAdjunto(models.Model):
    mensaje = models.ForeignKey(MensajeUsuario, on_delete=models.CASCADE)
    archivo = models.FileField(upload_to=archivo_mensaje)
