# Estas son las vistas que tienen permitido los usuarios
# de tipo Organizacion->Organizador
#
#
#

# Primero generamos una vista
# luego ponemos decorador solo si es con login
# luego vamos regulando a solo grupo
# luego, visualizar solo aquellas de los
# cursos con inscripcion activa

from apps.inscripcion.models import Inscripcion
from django.views.generic import ListView
from django.views.generic import TemplateView

#
#
# Home para Organizacion
#
# ver pantalla principal
# bloques seleccionados
# Postulaciones
# Estudiantes
# Cursos
# Profesores

class HomeOrganizacion(TemplateView):
    template_name = "organizacion/home.dj.html"

    def get_context_data(self, **kwargs):
        # obtener usuario 
        # si no está logueado ->404
        # si está logueado entregar Home de Org por defecto
        # mostrar menú de perfil y settings
        kwargs.setdefault('view', self)
        if self.extra_context is not None:
            kwargs.update(self.extra_context)
        return kwargs



#
# 
# Postulaciones
#

class PostulacionesLista(ListView):
    allow_empty = True
    template_name = "postulaciones/lista.dj.html"
    model = Inscripcion
