#
# Esta clase permite gestionar
# aquellos usuarios que sean de la organizacion x
# y les asigna permisos de grupo
#
#
from django.contrib.auth.models import User
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

def logo_org_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'organizacion/logo_{0}/{1}'.format(instance.nombre, filename)


class Organizacion(models.Model):
    OPCIONES=[
        ('tl','Taller',),
        ('cr','Curso'),
        ('ag','Agrupacion'),
        ('fn','Fundacion'),
        ('ong','ONG'),
        ('corp','Corporacion'),
        ('coop','Cooperativa'),
        ('snd','Sindicato'),
        ('emp','Emprendimiento'),]
    tipo = models.CharField(max_length=10, choices=OPCIONES)
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    logo = models.FileField(upload_to=logo_org_path)
    editar_grupo = models.BooleanField(default=True)
    administrar_usuarios = models.BooleanField(default=True)
    editar_curso=models.BooleanField(default=True)

class OrganizacionUser(models.Model):
    organizacion = models.ForeignKey(Organizacion,
                                     on_delete=models.SET('Borrada'))
    usuario = models.ForeignKey(User,
                                on_delete=models.SET('Borrad@'))
    nivel = models.IntegerField(default=3,
                                validators=[
                                    MaxValueValidator(100),
                                    MinValueValidator(1)
                                ])
