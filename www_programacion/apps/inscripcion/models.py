from django.db import models
from django.utils.text import slugify
import ujson as json
from phonenumber_field.modelfields import PhoneNumberField
from apps.proyecto.models import Proyecto
from markdownx.models import MarkdownxField

from django.contrib.auth.models import User


def get_upload_inscripcion_file_name(instance, filename):
    sg = instance.proyecto.slug_generacion
    return "inscripcion/patrocinio/%s/%s" % (sg, filename)


class ChequeoCodigo(models.Model):
    valor_aleatorio = models.CharField(max_length=10)

    class Meta:
        app_label = "inscripcion"
        verbose_name = "Código de Chequeo"
        verbose_name_plural = "Códigos de Chequeo"
        ordering = ('valor_aleatorio', )

    def __repr__(self):
        return json.dumps(self.__dict__)

    def __str__(self):
        return self.valor_aleatorio


class Inscripcion(models.Model):
    ON_REVIEW = "on_review"
    ACCEPTED = "accepted"
    REJECTED = "rejected"
    DEFAULT_MSG = "Tu postulación fue..."
    ACEPTA_DEFAULT = "¡Has sido aceptad@!"
    RECHAZA_DEFAULT = "¡Has sido rechazad@!"
    STATUS_CHOICES = [(ON_REVIEW, "Revisión"), (ACCEPTED, "Aceptada"),
                      (REJECTED, "Rechazada")]
    usuario = models.ForeignKey(User,
                                related_name="inscripciones",
                                on_delete=models.SET_NULL,
                                null=True)
    representa_organizacion = models.BooleanField(default=False)
    organizacion = models.CharField(max_length=100, blank=True)
    web_organizacion = models.URLField(max_length=200,
                                       default="http://sinweb.com")
    carta_patrocinio = models.FileField(
        upload_to=get_upload_inscripcion_file_name, blank=True)
    motivacion = models.TextField()
    proyecto = models.ForeignKey(Proyecto,
                                 related_name="inscripciones",
                                 on_delete=models.CASCADE,
                                 blank=True,
                                 null=True)
    valor_aleatorio = models.CharField(max_length=10)
    fecha_inscripcion = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=20,
                              choices=STATUS_CHOICES,
                              default=ON_REVIEW)

    def get_absolute_url(self):
        return reverse('inscripcion:persona',
                       kwargs={
                           'slug_curso': self.proyecto.curso.slug_nombre,
                           'slug_generacion': self.proyecto.slug_generacion
                       })

    @property
    def persona(self):
        if self.usuario:
            return "%s %s %s- %s - %s" % (
                self.usuario.first_name, self.usuario.profile.father_last_name,
                self.usuario.profile.mother_last_name,
                self.usuario.profile.email, self.usuario.profile.phonenumber)
        else:
            return "Sin usuario"

    @property
    def nombre_completo(self):
        if self.usuario:
            return "%s %s %s" % (self.usuario.profile.first_name,
                                 self.usuario.profile.father_last_name,
                                 self.usuario.profile.mother_last_name)
        else:
            return "Sin usuario"

    class Meta:
        app_label = "inscripcion"
        verbose_name = "Inscripción"
        verbose_name_plural = "Inscripciones"
        ordering = (
            '-fecha_inscripcion',
            "usuario",
            'proyecto',
        )
        permissions = [("can_apply", "Puede inscribirse")]

    def __repr__(self):
        return f"Inscripcion({self.proyecto.curso}, {self.proyecto}, {self.usuario})"

    def __str__(self):
        return self.persona

    @property
    def css_status(self):
        return f"color-{self.status}"

    @property
    def username(self):
        return self.usuario.username


from django.contrib.postgres.fields import JSONField


class RevisionInscripcion(models.Model):
    def DEFAULT_JSON():
        return dict(usuario=[], admins=[])

    DEFAULT_MSG = "Tu postulación fue..."
    ACEPTA_DEFAULT = "¡Has sido aceptad@!"
    RECHAZA_DEFAULT = "¡Has sido rechazad@!"
    promesa = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date_time = models.DateTimeField(auto_now=True)
    inscripcion = models.ForeignKey(Inscripcion, on_delete=models.CASCADE)
    mensaje = models.TextField(default=DEFAULT_MSG, blank=True)
    enviados = JSONField(default=DEFAULT_JSON)

    class Meta:
        app_label = "inscripcion"
        verbose_name = "Revisión de Inscripción"
        verbose_name_plural = "Revisiones de Inscripciones"
        ordering = (
            '-date_time',
            "inscripcion",
            'user',
        )
        permissions = [("can_review", "Puede revisar")]

    def __str__(self):
        return f"{self.user.profile}->{self.inscripcion}:{self.date_time}"

    def __repr__(self):
        return f"{self.user.profile}->{self.inscripcion}:{self.date_time}"
