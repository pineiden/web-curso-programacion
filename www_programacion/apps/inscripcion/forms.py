from django.forms import widgets
from django.utils import timezone
from networktools.library import my_random_string
from django.forms import ModelForm
from django import forms
from markdownx.fields import MarkdownxFormField
from phonenumber_field.formfields import PhoneNumberField
from apps.proyecto.models import Proyecto
from django.conf import settings
from captcha.fields import CaptchaField
from django.contrib.auth.models import User
import base64

from django.urls import reverse, reverse_lazy
from apps.webmail.tasks import send_mail
from apps.inscripcion.models import Inscripcion, RevisionInscripcion
from apps.inscripcion.models import ChequeoCodigo
from apps.proyecto.models import GrupoProyecto, get_member

ADMIN_EMAILS = getattr(settings, "ADMIN_EMAILS", None)
EMAIL_HOST_USER = getattr(settings, "EMAIL_HOST_USER", None)


def check_username(username):
    user = User.objects.get(username=username)
    return username


class InscripcionForm(ModelForm):
    """
    The formulary is displayed only with user authenticated
    """
    captcha = CaptchaField()
    representa_organizacion = forms.BooleanField(
        required=False,
        initial=False,
        label='¿Te apoya alguna organización comunitaria?')
    organizacion = forms.CharField(
        max_length=100,
        required=False,
        help_text="Solo se requiere si le apoya una organización")
    web_organizacion = forms.URLField(max_length=100,
                                      initial="http://www.sinweb.com",
                                      required=False)
    carta_patrocinio = forms.FileField(
        required=False,
        help_text="Solo se requiere si le apoya una organización")
    motivacion = forms.CharField(max_length=1500,
                                 label='¿Por qué deseas participar?',
                                 widget=forms.Textarea())
    valor_aleatorio = forms.CharField(max_length=10,
                                      widget=forms.HiddenInput())
    proyecto = forms.CharField(max_length=100, widget=forms.HiddenInput())
    usuario = forms.CharField(max_length=100, widget=forms.HiddenInput())

    class Meta:
        model = Inscripcion
        fields = [
            "representa_organizacion", "organizacion", "web_organizacion",
            "carta_patrocinio", "motivacion", "proyecto", "valor_aleatorio",
            "usuario"
        ]

    def send_usuario_email(self, *args, **kwargs):
        """
        kwargs keys => {from to subject body, template_info}
        template_info => {nivel_nombre nombre_completo fecha_inicio}
        """
        email_templates = self.instance.proyecto.email_templates
        fields = kwargs.get('fields')
        template = email_templates.load_template_admins(**kwargs)
        if fields:
            del kwargs['fields']
        opts = {
            'template_info': kwargs,
            'template_path': template,
            'debug': True
        }
        fields.update(opts)
        try:
            send_mail.delay(fields, template, kwargs, debug=True)
        except Exception as e:
            raise e

    def send_admin_email(self, *args, **kwargs):
        """
        kwargs keys => {from to subject body, template_info}
        template_info => {data_persona}
        """
        group = self.instance.proyecto.group
        members = group.get_members_dict()
        emails = set()
        for group_name, member_group in members.items():
            emails |= {member.email for member in member_group}
        kwargs["fields"]["to"] += list(emails)
        email_templates = self.instance.proyecto.email_templates
        fields = kwargs.get('fields')
        if fields:
            kwargs.pop('fields')
        data_persona = kwargs.get("data_persona")
        template = email_templates.load_template_admins(**data_persona)
        opts = {'template_info': kwargs, 'template_path': template}
        fields.update(opts)
        try:
            send_mail.delay(fields, template, kwargs, debug=True)
        except Exception as e:
            raise e

    def clean(self):
        cleaned_data = super().clean()
        representa_organizacion = cleaned_data['representa_organizacion']
        campos = {'organizacion', 'carta_patrocinio'}
        proyecto_id = cleaned_data.get('proyecto')
        usuario_id = cleaned_data.get('usuario')
        usuario = User.objects.get(id=usuario_id)
        proyecto = Proyecto.objects.get(id=proyecto_id)
        cleaned_data["usuario"] = usuario
        cleaned_data["proyecto"] = proyecto
        if representa_organizacion:
            self.fields_required(campos)
        else:
            cleaned_data.update({campo: '' for campo in campos})
        return cleaned_data

    def fields_required(self, fields):
        """Used for conditionally marking fields as required."""
        for field in fields:
            if not self.cleaned_data.get(field, ''):
                msg = forms.ValidationError("Se requiere este campo -> %s" %
                                            field)
                self.add_error(field, msg)


class InscripcionPostulanteForm(ModelForm):
    mail_fields = ('subject', 'body', 'from', 'to')
    status = forms.ChoiceField(choices=Inscripcion.STATUS_CHOICES)

    class Meta:
        model = RevisionInscripcion
        fields = ["status", "mensaje", "promesa", "inscripcion", "user"]
        widgets = {
            "inscripcion": widgets.HiddenInput(),
            "user": widgets.HiddenInput()
        }

    def add_user_to_group(self):
        # member: inscripcion.user.member
        # group: inscripcion.group
        # group -> get subgrup estudiante
        # subgroup assign_object member (dar membresía 'Estudiante')
        usuario = self.instance.inscripcion.usuario
        member = get_member(usuario)
        group = self.instance.inscripcion.proyecto.group
        subgrupo = group.sub_grupos(names=["Estudiante"])[0]
        if subgrupo:
            subgrupo.assign_object(member)

    def send_user_email(self):
        """
        Send result
        """
        revision = self.instance
        inscripcion = revision.inscripcion
        emails = inscripcion.proyecto.email_templates
        usuario = inscripcion.usuario
        proyecto = inscripcion.proyecto
        status = inscripcion.status
        curso = proyecto.curso
        fn = getattr(emails, f"load_template_{status}")
        kwargs = {}
        context = {
            "nombre_completo": str(usuario.profile),
            "nombre": f"{curso}|{proyecto}",
            "mensaje_particular": self.instance.mensaje
        }
        user_fields = dict(
            zip(self.mail_fields, [
                f"Resultado de su inscripción al curso {curso}|{proyecto}",
                "Este es el resultado de...",
                settings.DEFAULT_FROM_EMAIL,
                [inscripcion.usuario.email],
            ]))
        if fn:
            template = fn(**context)
            if not revision.enviados:
                revision.enviados = {"usuario": [], "admins": []}
            revision.enviados["usuario"].append({
                "datetime":
                timezone.now().isoformat(),
                "info":
                user_fields,
                "template":
                template
            })
            revision.save()
            send_mail.delay(user_fields, template, kwargs, debug=True)

    def send_admins_email(self):
        revision = self.instance
        inscripcion = revision.inscripcion
        group = inscripcion.proyecto.group
        proyecto = inscripcion.proyecto
        curso = proyecto.curso
        members = group.get_members_dict()
        emails = set()
        for group_name, member_group in members.items():
            emails |= {member.email for member in member_group}
        email_templates = proyecto.email_templates
        context = {"revision": revision, "inscripcion": revision.inscripcion}
        template = email_templates.load_template_result_admins(**context)
        email_list = set(settings.ADMIN_EMAIL_LIST) | emails
        email_list.add(revision.user.email)  # set add avoid email repetition
        admin_fields = dict(
            zip(self.mail_fields, [
                f"Resultado de su inscripción al curso {curso}|{proyecto}",
                "Se ha enviado el resultado de...",
                settings.DEFAULT_FROM_EMAIL,
                list(email_list),
            ]))
        if not revision.enviados:
            revision.enviados = {"usuario": [], "admins": []}
        revision.enviados["admins"].append({
            "datetime":
            timezone.now().isoformat(),
            "info":
            admin_fields,
            "template":
            template
        })
        try:
            revision.save()
        except Exception as e:
            print(f"Exception on save {{e}}")
            raise e
        kwargs = {}
        send_mail.delay(admin_fields, template, kwargs, debug=True)
