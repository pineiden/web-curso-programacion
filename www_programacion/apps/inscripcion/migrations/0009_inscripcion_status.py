# Generated by Django 3.0 on 2020-09-06 20:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inscripcion', '0008_auto_20200906_1701'),
    ]

    operations = [
        migrations.AddField(
            model_name='inscripcion',
            name='status',
            field=models.CharField(choices=[('on_review', 'Revisión'), ('accepted', 'Aceptada'), ('rejected', 'Rechazada')], default='on_review', max_length=20),
        ),
    ]
