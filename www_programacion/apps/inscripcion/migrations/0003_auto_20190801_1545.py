# Generated by Django 2.2.3 on 2019-08-01 15:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inscripcion', '0002_auto_20190731_0130'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='inscripcion',
            options={'ordering': ('apellido_materno', 'apellido_paterno', 'nombre', 'proyecto'), 'verbose_name': 'Inscripción', 'verbose_name_plural': 'Inscripciones'},
        ),
        migrations.AlterField(
            model_name='inscripcion',
            name='apellido_materno',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='inscripcion',
            name='apellido_paterno',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='inscripcion',
            name='nombre',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='inscripcion',
            name='organizacion',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='inscripcion',
            name='telefono',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='inscripcion',
            name='web_organizacion',
            field=models.URLField(),
        ),
    ]
