from networktools.environment import get_env_variable
"""
Using the django module ::

django-anymail

sendinblue_api_v3 = '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'
ANYMAIL = {
    'SENDINBLUE_API_KEY': sendinblue_api_v3
}
EMAIL_BACKEND = "anymail.backends.sendinblue.EmailBackend"
SENDINBLUE_API_URL = "https://api.sendinblue.com/v3/"
"""

EMAIL_HOST_USER = get_env_variable('EMAIL')
EMAIL_HOST_PASSWORD = get_env_variable('EMAIL_PASSWORD')
EMAIL_HOST = get_env_variable('EMAIL_HOST')
EMAIL_PORT = get_env_variable('EMAIL_PORT')
EMAIL_SUBJECT_PREFIX = "[CursoDeProgramacion]"
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'cursodeprogramacion@disroot.org'
CONTACT_EMAIL = DEFAULT_FROM_EMAIL
ADMIN_EMAIL_LIST = get_env_variable('ADMIN_EMAIL_LIST').split(":")

# for mail in ADMIN_EMAIL_LIST:
#    print(f"Admin email {mail}")
