from .base import *
from .installed import INSTALLED_APPS

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media',
                "files.functions.context_settings.parameters"
            ],
            'builtins': [],
            'libraries': {
                'new_tags': 'files.tags.new_tags',
                'filter_numeric': 'files.filters.numeric'
            }
        },
    },
]

ALLOWED_HOSTS = ['localhost', 'cursodeprogramacion.cl', '*']

STATIC_FILES = 'django.contrib.staticfiles'
if STATIC_FILES in INSTALLED_APPS:
    position = INSTALLED_APPS.index(STATIC_FILES)
    INSTALLED_APPS.insert(position, 'livereload')
    MIDDLEWARE.append('livereload.middleware.LiveReloadScript')

DEBUG = True

DBNAME = get_env_variable('DBNAME')
DBUSER = get_env_variable('DBUSER')
DBPASS = get_env_variable('DBPASS')
engine = 'django.contrib.gis.db.backends.postgis'
DATABASES = {
    'default': {
        'ENGINE': engine,
        'NAME': DBNAME,
        'USER': DBUSER,
        'PASSWORD': DBPASS,
        'HOST': '',
        'PORT': '',
    },
}

SITE_URL = "www.cursodeprogramacion.cl"
SITE_NAME = "Curso de Programación para Organizaciones Comunitarias"
GOOGLE_API_KEY = ''
SITE_ID = 1
SECURE_SSL_REDIRECT = False
CSRF_COOKIE_HTTPONLY = False

ADMIN_EMAILS = [
    'dpineda@uchile.cl',
]

CORS_ORIGIN_ALLOW_ALL = False

CORS_ALLOWED_ORIGINS = [
    "https://www.cursodeprogramacion.cl", "https://cursodeprogramacion.cl",
    "http://localhost:8000", "http://127.0.0.1:8000"
]

SITE_URL = 'localhost:8000'
