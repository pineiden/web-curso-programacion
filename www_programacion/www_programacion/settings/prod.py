from .base import *

if 'livereload' in INSTALLED_APPS:
    INSTALLED_APPS.remove('livereload')

IS_DEBUG = get_env_variable('IS_DEBUG')
DEBUG = False
if IS_DEBUG == 'DEBUG':
    DEBUG = True
# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
ALLOWED_HOSTS = ['localhost', 'cursodeprogramacion.cl', '*', '45.55.45.60']

DBNAME = get_env_variable('DBNAME')
DBUSER = get_env_variable('DBUSER')
DBPASS = get_env_variable('DBPASS')
engine = 'django.contrib.gis.db.backends.postgis'
DATABASES = {
    'default': {
        'ENGINE': engine,
        'NAME': DBNAME,
        'USER': DBUSER,
        'PASSWORD': DBPASS,
        'HOST': '',
        'PORT': '',
    },
}

SITE_URL = "www.cursodeprogramacion.cl"
SITE_NAME = "Curso de Programación"
GOOGLE_API_KEY = ''

SITE_ID = 1

#SECURE_SSL_REDIRECT = True
#SECURE_REDIRECT_EXEMPT = [r'^(?!admin/).*']
#USE_X_FORWARDED_HOST = True
# SESSION_COOKIE_SECURE=True
# CSRF_COOKIE_SECURE=True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media',
                "files.functions.context_settings.parameters"
            ],
            'builtins': [],
            'libraries': {
                'new_tags': 'files.tags.new_tags',
                'filter_numeric': 'files.filters.numeric'
            }
        },
    },
]

CORS_ORIGIN_ALLOW_ALL = False

CORS_ALLOWED_ORIGINS = [
    "https://www.cursodeprogramacion.cl",
    "https://cursodeprogramacion.cl",
]
