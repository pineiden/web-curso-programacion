"""
WSGI config for gabriela project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
from networktools.environment import get_env_variable

MODO = get_env_variable('MODO')
os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                      'www_programacion.settings.%s' % MODO)

application = get_wsgi_application()
