"""gabriela URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from apps.home.views import (page_not_found, bad_request, permission_denied,
                             server_error)
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('404/', page_not_found),
    path('403/', permission_denied),
    path('400/', bad_request),
    path('500/', server_error),
    path('grappelli/', include('grappelli.urls')),
    path('admin/', admin.site.urls),
    path('cuentas/',
         include('apps.accounts.urls', namespace='accounts'),
         name='accounts'),
    path('perfil/',
         include('apps.perfil_usuario.urls', namespace='perfil'),
         name='perfil'),
    path('', include('apps.home.urls', namespace='home'), name='home'),
    path('pagina/',
         include('apps.pagina.urls', namespace='pagina'),
         name='pagina'),
    path('inscripcion/',
         include('apps.inscripcion.urls', namespace='inscripcion'),
         name='inscripcion'),
    path('contacto/',
         include('apps.contacto.urls', namespace='contacto'),
         name='contacto'),
    path('proyecto/',
         include('apps.proyecto.urls', namespace='proyecto'),
         name='proyecto'),
    path('organizacion/',
         include('apps.organizacion.urls', namespace='organizacion'),
         name='organizacion'),
    path('captcha/', include('captcha.urls'), name='captcha'),
    path("admin_group/",
         include('apps.admin_group.urls', namespace='admin_group'),
         name='admin_group'),
    path('groups_manager/',
         include('groups_manager.urls', namespace='groups_manager'),
         name='groups_manager'),
    path("contenido/",
         include("apps.contenido.urls", namespace='contenido'),
         name='contenido')
]

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
