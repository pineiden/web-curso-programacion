function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            let cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function update_fieldq(username, fields, url, callback){
    /*
      username: nombre d 
usuario
      field_name: nombre del campo a modificar
      value: valor modificado
      field: campo

    */
    var csrftoken = getCookie('csrftoken');
    let all_data = {};
    for (let i in fields){
        let field=fields[i];
        field.classList.add("is-danger");
        let data = {
            username:username,
            field_name:field.name,
            value:field.value,
            csrfmiddlewaretoken: csrftoken
        };
        all_data[i]=(data);
    }
    let xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.onload = function result_update_field(event){
        let data_txt = xhr.responseText;
        let data_json = JSON.parse(data_txt);
        if (xhr.readyState==4){
            if (xhr.status==200){
                if (Object.keys(data_json).length>0){
                    for (let elem in data_json){
                        let field_name=data_json[elem].field_name;
                        for (let field in fields){
                            if (field_name===fields[field].name){
                                if(callback){
                                    callback(data_json);
                                }
                                else{
                                    fields[field].classList.remove("is-danger");
                                    fields[field].classList.add("is-success");}}
                        }
                    }
                }

            }
            else{
                console.log("Error al recibir");
            }
        }

    };

    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.setRequestHeader("X-CSRFToken", csrftoken);
    let json_string =JSON.stringify(all_data);
    console.log("Json string", json_string);
    xhr.send(json_string);
}
