let captcha_field = document.getElementById('captcha-label');

let refresh_button=document.createElement('BUTTON');
refresh_button.setAttribute('class', "button is-small is-rounded is-warning is-light group-captcha is-fullwidth");
refresh_button.addEventListener('click',refresh_captcha);

refresh_button.innerHTML = "<i class='fas fa-recycle'></i>Refrescar Captcha";
refresh_button.setAttribute('id','refresh-button');
refresh_button.type="button";

//captcha_field.parentNode.insertBefore(refresh_button, captcha_field);

//document.querySelectorAll("img.captcha");
let img_captcha=document.querySelectorAll("img.captcha")[0];
img_captcha.setAttribute('class','captcha group-captcha');
img_captcha.setAttribute('id','img-captcha');

let fragment = document.createDocumentFragment();
fragment.appendChild(img_captcha);


var captcha_image_comp=document.getElementById("captcha_image");
console.log("Agregando fragment", fragment);
captcha_image_comp.appendChild(fragment);
var refresh_button_comp=document.getElementById("refresh_button");
refresh_button_comp.appendChild(refresh_button);
// Insert text
//captcha_field.appendChild(refresh_button);


let url_captcha= location.protocol + "//" +
                 window.location.hostname + ":" +
                 location.port + "/captcha/refresh/";

let id_captcha_0="captcha-field-registro_0";

function refresh_captcha(){
    let xhr = new XMLHttpRequest();
    xhr.open('GET', url_captcha, true);
    xhr.onload = function recv_captcha_load(event){
        let data_txt = xhr.responseText;
        let data_json = JSON.parse(data_txt);

        if (xhr.readyState==4){
            if (xhr.status==200){
                let captcha_img=document.getElementsByClassName('captcha')[0];
                captcha_img.setAttribute('src', data_json['image_url']);
                document.getElementById(id_captcha_0).value=data_json['key'];
            }
            else{
                console.log("Error al recibir");
            }
        }

    };

    var csrftoken = getCookie('csrftoken');
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.setRequestHeader("X-CSRFToken", csrftoken);
    xhr.send();
}

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            let cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

