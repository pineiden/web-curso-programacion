import './index.scss';
const bulmaCarousel = require('../node_modules/bulma-extensions/bulma-carousel/dist/js/bulma-carousel.min.js');
const bulmaAccordion =require('../node_modules/bulma-extensions/bulma-accordion/src/js/index.js').default;
const bulmaCalendar = require('../node_modules/bulma-extensions/bulma-calendar/dist/js/bulma-calendar.min.js');

 window.addEventListener("load", function(event) {
     console.log("'Todos los recursos terminaron de cargar!", bulmaAccordion);
     let accordions = bulmaAccordion.attach();
     console.log(accordions);
     console.log(bulmaCalendar);
  });

window.modal_thumb = function(modal_id){
    let this_modal = document.getElementById(modal_id);
    let close_button = document.getElementById(modal_id+"_close");
    close_button.addEventListener("click", event=>{
        this_modal.classList.remove("is-active");
    });
};


function read_only(cal){
        // removes the initial click listeners
        // (only works before month is changed)
        cal.datePicker._ui.days.forEach(day => {
            day.removeEventListener('click', cal.datePicker.onDateClickDatePicker);
            day.removeEventListener('touch', cal.datePicker.onDateClickDatePicker);
        });
        // removes click callback that works for all but the first click
        cal.datePicker.onDateClickDatePicker = null;
}

window.activate_calendar = function (element_id, destiny, options){
    let default_options = {
        type:"datetime",
        isRange:true,
        allowSameDayRange:true,
        dateFormat:"DD/MM/YYYY",
        timeFormat:"HH:mm:SS",

    };
    if (!options){
        options=default_options;
    }else{
        const opts = {...options, ...default_options};
        Object.keys(opts).forEach(key=>{
            if (key in options){
                opts[key]=options[key];
            }
        });
        options=opts;
    };
    // Initialize all input of date type.
    const calendars = bulmaCalendar.attach("#"+element_id, options);

    // Loop on each calendar initialized
    calendars.forEach(calendar => {
	      // Add listener to date:selected event
	      calendar.on('date:selected', date => {
            console.log("Date selected", date);
	      });
        if (options.read_only){
            console.log("Setting read_only", element_id);
            read_only(calendar);
        }
    });

    // To access to bulmaCalendar instance of an element
    const element = document.querySelector('#'+element_id);
    if (element) {
	      // bulmaCalendar instance is available as element.bulmaCalendar
	      element.bulmaCalendar.on('select', datepicker => {
            let start=datepicker.data.startDate;
            let end=datepicker.data.endDate;
            let element_start = document.getElementById(destiny[0]);
            let element_end = document.getElementById(destiny[1]);
            element_start.value = start.toLocaleDateString("es-ES");
            element_end.value = end.toLocaleDateString("es-ES");
	      });
    }
}

window.show_thumb_image = function (input, element_id){
        if(input.files && input.files[0]){
            var reader = new FileReader();
            let file = input.files[0];
            reader.onload =function(e){
                let element = document.getElementById(element_id);
                let element_modal = document.getElementById(element_id+"_x_img");
                element.setAttribute('src', e.target.result);
                element.setAttribute('alt', file.name);
                if (element_modal){
                    element_modal.setAttribute('src', e.target.result);
                    element_modal.setAttribute('alt', file.name);
                }
            };
            //finally read the file from source
            reader.readAsDataURL(file);
        }
    };


window.getCookie = function (name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            let cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

window.update_field=function update_field(username, fields, url, is_file, callback){
    /*
      username: nombre d 
usuario
      field_name: nombre del campo a modificar
      value: valor modificado
      field: campo

    */
    var csrftoken = getCookie('csrftoken');
    let all_data = {};
    let xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.onload = function result_update_field(event){
        let data_txt = xhr.responseText;
        let data_json = JSON.parse(data_txt);
        if (xhr.readyState==4){
            if (xhr.status==200){
                if (Object.keys(data_json).length>0){
                    for (let elem in data_json){
                        let field_name=data_json[elem].field_name;
                        for (let field in fields){
                            if (field_name===fields[field].name){
                                if(callback){
                                    callback(data_json);
                                }
                                else{
                                    fields[field].classList.remove("is-danger");
                                    fields[field].classList.add("is-success");}}
                        }
                    }
                }

            }
            else{
                console.log("Error al recibir");
            }
        }

    };

    let file;
    if (is_file){
        var reader = new FileReader();
    }
    for (let i in fields){
        let field=fields[i];
        console.log("field", field);
        field.classList.add("is-danger");
        console.log("Field", field);
        let data = {
            username:username,
            field_name:field.name,
            value:field.value,
            csrfmiddlewaretoken: csrftoken};

        if (field.type==="file"){
            console.log("Es archivo extrayendo archivo...");
            reader.onloadend = function () {
                data.blob = reader.result;
                console.log("Set data blob", data.blob, reader.result);
                let json_string =JSON.stringify(all_data);
                console.log("Json string", json_string);
                xhr.send(json_string);
            };
            file=field.files[0];
        }

        all_data[i]=data;
    }

    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.setRequestHeader("X-CSRFToken", csrftoken);
    if (!is_file){
        let json_string =JSON.stringify(all_data);
        xhr.send(json_string);}
    else{
        reader.readAsDataURL(file);
    }
}


window.active_carousel = function active_carousel(id_name, options){
    bulmaCarousel.attach(id_name, options);
};
