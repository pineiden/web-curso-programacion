import copy


def union_complemento(base, nueva_info):
    """
    nueva_info + (base-nueva_info)
    """
    print("Base", base)
    print("Nueva info", nueva_info)
    union_dict = copy.deepcopy(base)
    union_dict.update(nueva_info)
    print("Resultado...", union_dict)
    return union_dict
