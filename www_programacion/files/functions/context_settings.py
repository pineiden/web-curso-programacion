from django.conf import settings


def parameters(request):
    data = {
        "SITE_NAME": settings.SITE_NAME,
        "SITE_URL": settings.SITE_URL,
        "MENU_NAV": settings.MENU_NAV,
        "CONTACT_EMAIL": settings.CONTACT_EMAIL
    }
    return data
