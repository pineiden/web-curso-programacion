#+TITLE: Nosotros

* Resumen

Una descripción sobre quienes participamos en el proyecto

* Presentación

Tenemos distintos orígenes, algunos somos estudiantes, otros trabajadores o
trabajadoras que tenemos conocimientos en tecnologías y deseamos compartir de
manera comunitaria y colaborativa. 

Decidimos participar en la construcción y desarrollo del proyecto «Curso de Programación para Organizaciones Comunitarias».
Hemos realizado tres versiones del curso (y seguimos actualizando), participaron
estudiantes de diversas organizaciones sociales comunitarias, entre nosotres la
colaboración de personas que han participado en el curso es de suma importancia,
trabajamos como equipo. 

Nuestro contenido consiste en proyectar conocimientos, habilidades y técnicas a
ciertos lugares de la sociedad donde comunmente este tipo de información es casi
nula o inexistente. Como parte de la sociedad nuestro objetivo estructural
consiste en la construcción de redes comunitarias basadas en la tecnologías de la
información y comunicación. 

Este curso es bidireccional, porque los instructores tambíen aprenden de los
participantes, siendo este un sistema retroalimentativo. Brindamos a través del
aprendizaje que permiten crear una comunidad capaz de comprender para así crear
tecnologías. 

El bienestar de lo común, "tecnocomún" un término adoptado por nuestra comunidad
para dar referecia al desarrollo de las ideas basadas en las tecnologías,
nuestro entorno conceptual es netamente el desenvolvimiento de los conceptos
tecnológicos aplicado a lo comunitario. 

La creación de contenidos se lleva a cabo mediante la enseñanza a nuestros
participantes, el diseño y desarrollo de las distintas herramientas de difusión
y compartir los conocimientos en canales educativos de conversación, alentar a
las personas a familiarizarse con las nuevas tecnologías. 

Uno de los propósitos didácticos de este proyecto es llevarlo a *donde sea
necesario*, por ende existe la posibilidad de autogestionar una instancia del
mismo en cualquier lugar, siempre y cuando sea necesario. Esta plataforma (en
desarrollo) permitirá distribuir los contenidos según el curso o taller a
disposición que se den con motivo desde lo tecnocomún. 

Trabajamos también en la edición de los textos guía del curso y convertirlos en
un libro de contextura auténtica, autónoma y didáctica, para conceder la
facilitación en los procesos de aprendizaje en tecnologías libres. 

Somos el grupo de personas que decidimos participar en la construcción y
desarrollo del proyecto «Curso de Programación para Organizaciones Comunitarias».
